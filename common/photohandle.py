from PIL import Image
import xml.dom.minidom


# 传入旋转的角度，图片大小；返回旋转后的坐标（left,top,width,height)
# 角度正：顺时针旋转
def getrotate(xmin, ymin, xmax, ymax, photoWidth, photoHeight, angle):
    if angle == 0:
        return xmin, ymin, xmax, ymax, photoWidth, photoHeight
    elif angle == 90:
        newPhotoWidth = photoHeight
        newXmin = newPhotoWidth - ymax
        newXmax = newPhotoWidth - ymin
        newYmin = xmin
        newYmax = xmax
        return newXmin, newYmin, newXmax, newYmax
    elif angle == 180:
        newXmin = photoWidth - xmax
        newXmax = photoWidth - xmin
        newYmin = photoHeight - ymax
        newYmax = photoHeight - ymin
        return newXmin, newYmin, newXmax, newYmax
    elif angle == 270:
        newPhotoHeigth = photoWidth
        newXmin = ymin
        newXmax = ymax
        newYmin = newPhotoHeigth - xmax
        newYmax = newPhotoHeigth - xmin
        return newXmin, newYmin, newXmax, newYmax
    else:
        return Exception('不支持此旋转')


# 替换以nodeName为名的节点，新节点文本是text
def nodemodify(doc, parentNode, nodeName, text):
    node = parentNode.getElementsByTagName(nodeName)[0]
    newNode = doc.createElement(nodeName)
    text = doc.createTextNode(text)
    newNode.appendChild(text)
    parentNode.replaceChild(newNode, node)


# 更新xml文件
def setxml(file_path, xml_path, cleft, ctop, cwidth, cheight, scaleX, scaleY, angle):
    print('修改xml文件')

    # 获取图片大小
    im = Image.open(file_path)
    photoWidth = im.size[0]
    photoHeight = im.size[1]
    doc = xml.dom.minidom.parse(xml_path)  # 打开xml文件
    root = doc.documentElement  # 得到文档元素对象
    size = root.getElementsByTagName('size')[0]  # 获取图片大小节点
    nodemodify(doc, size, 'width', str(photoWidth))
    nodemodify(doc, size, 'height', str(photoHeight))

    objects = root.getElementsByTagName('object')  # 按标签名称查找，返回标签结点数组
    for object in objects:
        name = object.getElementsByTagName('name')[0]
        bndbox = object.getElementsByTagName('bndbox')[0]
        xmin = bndbox.getElementsByTagName('xmin')[0]
        ymin = bndbox.getElementsByTagName('ymin')[0]
        xmax = bndbox.getElementsByTagName('xmax')[0]
        ymax = bndbox.getElementsByTagName('ymax')[0]
        name = name.childNodes[0].data
        xmin = xmin.childNodes[0].data
        ymin = ymin.childNodes[0].data
        xmax = xmax.childNodes[0].data
        ymax = ymax.childNodes[0].data
        xmin = int(xmin)
        ymin = int(ymin)
        xmax = int(xmax)
        ymax = int(ymax)

        # 裁剪
        xmin = xmin - cleft
        ymin = ymin - ctop
        xmax = xmax - cleft
        ymax = ymax - ctop
        print(xmin, ymin, xmax, ymax)
        # 旋转
        xmin, ymin, xmax, ymax = getrotate(xmin, ymin, xmax, ymax, cwidth, cheight, angle)
        print(xmin, ymin, xmax, ymax)
        # 缩放
        xmin = int(xmin*scaleX)
        ymin = int(ymin*scaleY)
        xmax = int(xmax*scaleX)
        ymax = int(ymax*scaleY)
        print(xmin, ymin, xmax, ymax)
        # 修改节点信息
        nodemodify(doc, bndbox, 'xmin', str(xmin))
        nodemodify(doc, bndbox, 'ymin', str(ymin))
        nodemodify(doc, bndbox, 'xmax', str(xmax))
        nodemodify(doc, bndbox, 'ymax', str(ymax))

    # 写入到文件
    fd = open(xml_path, 'w', encoding='UTF-8')
    doc.writexml(fd, indent='', addindent='', newl='', encoding='UTF-8')


# 更新yolo文件
def setyolo(file_path, txt_path, sign_data, cleft, ctop, cwidth, cheight, scaleX, scaleY, angle):
    print('修改txt文件')

    # 获取图片大小
    im = Image.open(file_path)
    photoWidth = im.size[0]
    photoHeight = im.size[1]
    fd_txt = open(txt_path, 'w+')
    # 清空文件
    fd_txt.seek(0)
    fd_txt.truncate()
    for data in sign_data:
        xmin = data['xmin']
        ymin = data['ymin']
        xmax = data['xmax']
        ymax = data['ymax']
        sign = data['sign']

        # 裁剪
        xmin = xmin - cleft
        ymin = ymin - ctop
        xmax = xmax - cleft
        ymax = ymax - ctop
        print(xmin, ymin, xmax, ymax)
        # 旋转
        xmin, ymin, xmax, ymax = getrotate(xmin, ymin, xmax, ymax, cwidth, cheight, angle)
        print(xmin, ymin, xmax, ymax)

        # 缩放
        xmin = int(xmin * scaleX)
        ymin = int(ymin * scaleY)
        xmax = int(xmax * scaleX)
        ymax = int(ymax * scaleY)
        print(xmin, ymin, xmax, ymax)

        # 转化成yolo格式保存
        dw = 1. / photoWidth
        dh = 1. / photoHeight
        x = (xmin + xmax) / 2.0 - 1
        y = (ymin + ymax) / 2.0 - 1
        w = xmax - xmin
        h = ymax - ymin
        x = round(x * dw, 6)
        w = round(w * dw, 6)
        y = round(y * dh, 6)
        h = round(h * dh, 6)
        # 格式化数据
        data = "{sign} {x} {y} {w} {h}\n".format(sign=sign, x=x, y=y, w=w, h=h)
        print(data)
        fd_txt.write(data)
    fd_txt.close()