from files.models.File import File

# 将数据库中以字节为单位的数值，转换为b,M,G等


def getfilesize(size):
    Kb = int(1024)
    Mb = int(1024) * int(1024)
    Gb = Mb * int(1024)
    # size = int(fileObj.size)
    size = int(size)
    if size <= Kb:
        return str(size) + 'b'
    elif size <= Mb:
        result = size >> 10
        return str(result) + 'Kb'
    elif size <= Gb:
        result = size >> 20
        return str(result) + 'Mb'
    else:
        result = size >> 30
        return str(result) + 'Gb'


if __name__ == '__main__':
    a=235
    result = getfilesize(a)
    print(result)
