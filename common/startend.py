import math


# 分页显示，返回首尾下标
def startEnd(page, count, pagesize=20):
    maxPage = math.ceil(count / pagesize)
    if int(page) == maxPage:
        end = count
        start = (maxPage - 1) * pagesize
    elif int(page) < maxPage:
        end = int(page) * pagesize
        start = end - pagesize
    else:
        start = count
        end = count
    return start, end