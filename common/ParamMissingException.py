# -*- coding:utf-8 -*-
import logging

logger = logging.getLogger('django')
class ParamMissingException(Exception):
    def __init__(self, code=402, error=u'参数缺失', data=u'参数缺失'):
        self.code = code
        self.error = error
        self.data = data
        logger.error("%d %s : %s " % (self.code, self.error, self.data))


    def __str__(self):
        logger.error("%d %s : %s " %(self.code, self.error, self.data))
        return "%d %s : %s " %(self.code, self.error, self.data)
