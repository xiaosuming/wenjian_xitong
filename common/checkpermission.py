from files.models.Folder import Folder
from files.models.File import File
from django.contrib.auth.models import User
from files.models.Permission import Permission


# 检查用户是否对文件夹有权限
# 循环向上查找，父目录有权限，子目录及子文件都有权限
def foldercheckpermission(folderObj, user):

    while (folderObj != None):
        permissionObj = Permission.objects.filter(user=user, folder=folderObj).first()
        if permissionObj:
            return permissionObj.right
        folderObj = folderObj.parent
    return -1


# 检查用户对文件是否有权限
# 循环向上查找，父目录有权限，子目录及子文件都有权限
def filecheckpermission(fileObjList, user):
    rightList = []
    noRightList = []
    for fileObj in fileObjList:
        permissionObj = Permission.objects.filter(user=user, file=fileObj).first()
        if permissionObj is None:
            noRightList.append(fileObj)
        rightList.append(permissionObj.right)

    # 依次遍历无权限的文件的父目录，作为文件的权限
    if len(noRightList) != 0:
        for fileObj in noRightList:
            folderObj = fileObj.pfolder
            p = foldercheckpermission(folderObj, user)
            rightList.append(p)

    return rightList


# 检查用户对文件是否有权限
# 循环向上查找，父目录有权限，子目录及子文件都有权限
def onefilecheckpermission(fileObj, user):
    permissionObj = Permission.objects.filter(user=user, file=fileObj).first()
    print(permissionObj)
    if permissionObj is None:
        # 依次遍历无权限的文件的父目录，作为文件的权限
        folderObj = fileObj.pfolder
        p = foldercheckpermission(folderObj, user)
        right = p
    else:
        right = permissionObj.right
    return right


if __name__ == '__main__':
    # test
    username = 'sgf'
    user = User.objects.filter(username=username).first()
    folderObj = Folder.objects.filter(id=19).frist()
    right = foldercheckpermission(folderObj, user)
    print(right)