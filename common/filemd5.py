import hashlib
from functools import partial

def md5(data, block_size=65536):
    # 创建md5对象
    m = hashlib.md5()
    # 对django中的文件对象进行迭代
    for item in iter(partial(data.read, block_size), b''):
        # 把迭代后的bytes加入到md5对象中
        m.update(item)
    str_md5 = m.hexdigest()
    return str_md5
