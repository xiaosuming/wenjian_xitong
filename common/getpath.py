import os


# 得到绝对路径
# 从项目的路径开始
def getpath(*args):
    path = os.getcwd()
    for arg in args:
        path = os.path.join(path, arg)
    return path