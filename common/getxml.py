from common.getpath import getpath
import xml.dom.minidom
import os


# 获取缩放比例 显示/实际
def getratio(width, height):
    if width > 600 or height > 700:
        # 前端显示框比例高/宽=7/6
        r = height/width
        fixedratio = 7/6
        if r < fixedratio:
            ratio = 600/width
        else:
            ratio = 700/height
    else:
        ratio = 1
    return float(ratio)


# 获取voc数据集标记xml格式
# 返回标记方框的左上角坐标，宽和高，单位像素
def getxml(code, username):
    xmlName = code + '.xml'
    folder_url = os.path.join('sign', username, code)
    sign_url = getpath('media', folder_url)
    print('sign_url', sign_url)
    files = os.listdir(sign_url)
    files.remove(xmlName)
    print(files)
    photoName = files[0]
    print(photoName)
    download_url = os.path.join(folder_url, photoName)
    code_url = os.path.join(sign_url, xmlName)
    dom1 = xml.dom.minidom.parse(code_url)  # 打开xml文件
    root = dom1.documentElement  # 得到文档元素对象
    size = root.getElementsByTagName('size')[0]  # 获取图片大小
    photoWidth = size.getElementsByTagName('width')[0]
    photoWidth = photoWidth.childNodes[0].data
    photoHeight = size.getElementsByTagName('height')[0]
    photoHeight = photoHeight.childNodes[0].data
    ratio = getratio(int(photoWidth), int(photoHeight))
    print(ratio)
    objects = root.getElementsByTagName('object')  # 按标签名称查找，返回标签结点数组
    data = []
    for object in objects:
        name = object.getElementsByTagName('name')[0]
        bndbox = object.getElementsByTagName('bndbox')[0]
        xmin = bndbox.getElementsByTagName('xmin')[0]
        ymin = bndbox.getElementsByTagName('ymin')[0]
        xmax = bndbox.getElementsByTagName('xmax')[0]
        ymax = bndbox.getElementsByTagName('ymax')[0]
        name = name.childNodes[0].data
        xmin = xmin.childNodes[0].data
        ymin = ymin.childNodes[0].data
        xmax = xmax.childNodes[0].data
        ymax = ymax.childNodes[0].data
        style = {}
        style['name'] = name
        height = float(ymax) - float(ymin)
        height = int(height*ratio)
        width = float(xmax) - float(xmin)
        width = int(width*ratio)
        xmin = float(xmin)
        xmin = int(xmin*ratio)
        ymin = float(ymin)
        ymin = int(ymin*ratio)
        cubeStyle = {
            'height': str(height) + 'px',
            'width': str(width) + 'px',
            'left': str(xmin) + 'px',
            'top': str(ymin) + 'px',
        }
        style['cubeStyle'] = cubeStyle
        data.append(style)
    return data, download_url


# 得到标记框信息
def getrawxml(path):
    # 获取标记信息
    print('获取标记信息')
    dom1 = xml.dom.minidom.parse(path)  # 打开xml文件
    root = dom1.documentElement  # 得到文档元素对象
    objects = root.getElementsByTagName('object')  # 按标签名称查找，返回标签结点数组
    data = []
    for object in objects:
        name = object.getElementsByTagName('name')[0]
        bndbox = object.getElementsByTagName('bndbox')[0]
        xmin = bndbox.getElementsByTagName('xmin')[0]
        ymin = bndbox.getElementsByTagName('ymin')[0]
        xmax = bndbox.getElementsByTagName('xmax')[0]
        ymax = bndbox.getElementsByTagName('ymax')[0]
        name = name.childNodes[0].data
        xmin = xmin.childNodes[0].data
        ymin = ymin.childNodes[0].data
        xmax = xmax.childNodes[0].data
        ymax = ymax.childNodes[0].data
        cubeStyle = {
            'xmin': int(xmin),
            'ymin': int(ymin),
            'xmax': int(xmax),
            'ymax': int(ymax)
        }
        data.append(cubeStyle)
    return data


if __name__ == '__main__':
    path = os.getcwd()
    print(path)
    data = getxml('0.xml')
    print(data)
