#!/usr/bin/python
# -*- coding: UTF-8 -*-

import os


def folderdel(path):
    # 保存目录
    p = path
    # 递归遍历目录
    for root, dirs, files in os.walk(path, topdown=False):
        for name in files:
            path = os.path.join(root, name)
            os.remove(path)
        for name in dirs:
            path = os.path.join(root, name)
            os.rmdir(path)
    # 删除导入的目录
    os.rmdir(p)


if __name__ == '__main__':
    path = 'D:\\gj\\temp'
    folderdel(path)
