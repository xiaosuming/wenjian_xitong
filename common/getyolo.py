from common.getpath import getpath
from PIL import Image
import os


def getratio(width, height):
    if width > 600 or height > 700:
        # 前端显示框比例高/宽=7/6
        r = height/width
        fixedratio = 7/6
        if r < fixedratio:
            ratio = 600/width
        else:
            ratio = 700/height
    else:
        ratio = 1
    return float(ratio)

def getyolo(code, username):
    folder_url = os.path.join('sign', username, code)
    sign_url = getpath('media', folder_url)
    files = os.listdir(sign_url)
    print(files)
    files.remove(code+'.txt')
    files.remove('obj.names')
    photoName = files[0]
    photoPath = os.path.join(sign_url, photoName)
    download_url = os.path.join(folder_url, photoName)
    # 获取图片大小
    im = Image.open(photoPath, 'r')
    width, height = im.size
    # 显示比率
    ratio = getratio(int(width), int(height))
    photoTxt = os.path.join(sign_url, code+'.txt')
    fd_txt = open(photoTxt)
    photoSign = os.path.join(sign_url, 'obj.names')
    fd_name = open(photoSign)
    data = []
    for name, coordinate in zip(fd_name.readlines(), fd_txt.readlines()):
        mystr = coordinate.split(' ', 4)
        style = {
            'name': name.strip(),
        }
        xmin = (float(mystr[1]) - float(mystr[3])*0.5)*width + 1
        ymin = (float(mystr[2]) - float(mystr[4])*0.5)*height + 1
        height = (float(mystr[4])*height)
        width = (float(mystr[3])*width)
        xmin = int(xmin*ratio)
        ymin = int(ymin*ratio)
        height = int(height*ratio)
        width = int(width*ratio)
        cubeStyle = {
            'height': str(height) + 'px',
            'width': str(width) + 'px',
            'left': str(xmin) + 'px',
            'top': str(ymin) + 'px',
        }
        style['cubeStyle'] = cubeStyle
        data.append(style)

    fd_txt.close()
    fd_name.close()
    return data, download_url


if __name__ == '__main__':
    data = getyolo('f32ee1061665fc5b268ba1a2fb720e33', 'sgf')
    print(data)
