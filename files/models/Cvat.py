from django.db import models
from files.models.File import File
from django.contrib.auth.models import User

class Cvat(models.Model):
    file = models.ForeignKey(
        File,
        on_delete=models.CASCADE,
        verbose_name=u'文件id',
        # true为必填字段,加上null为true时，空值被储存为null
        blank=True,
        null=True,
    )
    task_id = models.IntegerField(
        verbose_name=u'cvat的task_id',
        blank=True,
        null=True,
    )
    create = models.ForeignKey(
        User,
        verbose_name=u'创建者',
        on_delete=models.CASCADE,
        related_name='cvat',
        null=True
    )

    class Meta:
        verbose_name = u'cvat关联表'
        verbose_name_plural = u'cvat关联表'

    def __str__(self):
        return u'%s 权限表' % self.create.username