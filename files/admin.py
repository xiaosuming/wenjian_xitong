from django.contrib import admin
from files.models.Cvat import Cvat
from files.models.File import File
from files.models.Folder import Folder
from files.models.Permission import Permission

admin.site.register(File)
admin.site.register(Folder)
admin.site.register(Permission)
admin.site.register(Cvat)