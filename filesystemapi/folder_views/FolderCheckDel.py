from files.models.Folder import Folder
from django.views.generic import View
from django.contrib.auth.models import User
from common.ParamMissingException import ParamMissingException
from common.checkpermission import foldercheckpermission
from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator


class FolderCheckDel(View):
    """
        @apiName FolderCheckDel
        @api {POST} /filesystem/foldercheckdel/
        @apiGroup check
        @apiVersion 0.0.1
        @apiDescription 删除文件夹时，检查文件夹是否有权限
        @apiParam {int} folderId 文件夹id
        @apiParamExample {form-data} 请求样例：
        {
            "folderId": 1,
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
        "code": 0,
        "msg": "SUCCESS",
        "action": "Check Delete Folder",
        "data": {
            "access": true,
            }
        }
    """
    @method_decorator(httpresult)
    def post(self, request):
        folderId = request.POST.get('folderId')

        # test
        username = 'sgf'
        user = User.objects.filter(username=username).first()

        if folderId is None:
            raise ParamMissingException(u'缺少目录位置信息')

        folderObj = Folder.objects.filter(id=folderId).first()
        if folderObj is None:
            raise Exception(u'此文件夹不存在')

        data = {}
        # 检查权限
        folderCreater = folderObj.creater
        if user != folderCreater:
            p = foldercheckpermission(folderObj, user)
            if p is not True:
                data['access'] = False
                return res(code=1, msg="ERROR", action="Check Delete Folder", data=data)
        data['access'] = True

        return res(action='Check Delete Folder', data=data)
