from django.views.generic import View
from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator
from .function.folderdownload import folderdownload

class FolderDownLoad(View):
    """
        @apiName FolderDownLoad
        @api {GET} /filesystem/folderdownload/
        @apiGroup folder
        @apiVersion 0.0.1
        @apiDescription 下载文件夹，返回返回压缩后的url
        @apiParam {int} folderId 文件夹id
        @apiParamExample {params} 请求样例：
        {
            "folderId":2
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Download Folder",
            "data": {
                "downloadUrl": xxx/xxx/xxx,
            }
        }
    """
    @method_decorator(httpresult)
    def get(self, request):
        folderId = request.GET.get('folderId')
        # test
        username = 'sgf'
        data = folderdownload(folderId, username)
        return res(action='Download Folder', data=data)





