from files.models.Folder import Folder
from files.models.File import File
from django.views.generic import View
from django.contrib.auth.models import User
from common.ParamMissingException import ParamMissingException
from common.checkpermission import foldercheckpermission
from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator


class FolderCheckRename(View):
    """
        @apiName FolderCheckRename
        @api {POST} /filesystem/foldercheckrename/
        @apiGroup check
        @apiVersion 0.0.1
        @apiDescription 重命名文件夹时，检查文件夹是否重名
        @apiParam {int} folderId 文件夹id
        @apiParam {string} folderName 文件名
        @apiParamExample {form-data} 请求样例：
        {
            "folderId": 1,
            "folderName": "helloworld",
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
        "code": 0,
        "msg": "SUCCESS",
        "action": "Check Rename Folder",
        "data": {
            "access": true,
            "duplicate": false
            }
        }
    """
    @method_decorator(httpresult)
    def post(self, request):
        folderId = request.POST.get('folderId')
        newFoldername = request.POST.get('folderName', '')

        # test
        username = 'sgf'
        user = User.objects.filter(username=username).first()

        if folderId is None:
            raise ParamMissingException(u'缺少目录位置信息')
        if newFoldername is None:
            raise ParamMissingException(u'缺少新建目录名')

        folderObj = Folder.objects.filter(id=folderId).first()
        if folderObj is None:
            raise Exception(u'此文件夹不存在')

        data = {}
        # 检查权限
        folderCreater = folderObj.creater
        if user != folderCreater:
            p = foldercheckpermission(folderObj, user)
            if p is not True:
                data['access'] = False
                return res(code=1, msg="ERROR", action="Check Rename Folder", data=data)
        data['access'] = True

        # 检查是否重名
        pfolder = folderObj.parent
        folderObjs = Folder.objects.filter(name=newFoldername, parent=pfolder)
        fileObjs = File.objects.filter(name=newFoldername, pfolder=pfolder)

        if folderObjs.count() == 0 and fileObjs.count() == 0:
            data['duplicate'] = False
            return res(action='Check Rename Folder', data=data)
        else:
            data['duplicated'] = True
            return res(code=1, msg='ERROR', action='Check Rename Folder', data=data)
