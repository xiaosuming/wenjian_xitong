from files.models.Folder import Folder
from files.models.File import File
from files.models.Permission import Permission
from django.contrib.auth.models import User
from django.views.generic import View
from common.ParamMissingException import ParamMissingException
from common.getpath import getpath
from common.httpresult import httpresult
from common.res import res
from common.folderdel import folderdel
from django.utils.decorators import method_decorator
from django.db.models import Q
import shutil
import os


class FolderCutPaste(View):
    """
          @apiName FolderCutPaste
          @api {POST} /filesystem/foldercutpaste/
          @apiGroup folder
          @apiVersion 0.0.1
          @apiDescription 文件夹移动
          @apiParam {int} srcFolderId 源文件夹id
          @apiParam {int} dstFolderId 目的文件夹id
          @apiParamExample {form-data} 请求样例：
          {
              "srcFolderId": 2,
              "dstFolderId": 3
          }
          @apiSuccess (200) {String} msg 信息
          @apiSuccess (200) {String} code 0代表无错误 1代表有错误
          @apiSuccess (200) {json} data
          @apiSuccessExample {json} 返回样例:
          {
              "code": 0,
              "msg": "SUCCESS",
              "action": "CutPaste Folder",
              "data": {
                "folderId": 2,
                "folderName": "hello",
                "depth": 3,
              }
          }
    """
    def getsubobj(self, folderId,pfolder, flag):
        print('进入递归函数')
        folder = Folder.objects.filter(id=folderId).first()
        if folder == None:
            return

        subfolderObjs = Folder.objects.filter(Q(parent=folder) & ~Q(name=folder.name))
        subfileObjs = File.objects.filter(pfolder=folder)

        print('subfile_count', subfileObjs.count())
        print('subfolder_count', subfolderObjs.count())
        if subfolderObjs.count() == 0 and subfileObjs.count() == 0:
            return
        if subfileObjs.count() != 0:
            for subfileObj in subfileObjs:
                print('file:', subfileObj)
                subfileObj.url = os.path.join(pfolder.url, subfileObj.name)
                subfileObj.creater = pfolder.creater
                subfileObj.depth = pfolder.depth + 1
                subfileObj.save()
                if flag == True:
                    permissionObjs = Permission.objects.filter(file=subfileObj, folder=None)
                    if permissionObjs.count != 0:
                        permissionObjs.delete()

        if subfolderObjs.count() != 0:
            # 复制该目录下的目录
            for subfolderObj in subfolderObjs:
                print('folder:', subfolderObj)
                id = subfolderObj.id
                subfolderObj.url = os.path.join(pfolder.url, subfolderObj.name)
                subfolderObj.creater = pfolder.creater
                subfolderObj.depth = pfolder.depth + 1
                subfolderObj.save()
                if flag == True:
                    permissionObjs = Permission.objects.filter(file=None, folder=subfolderObj)
                    if permissionObjs.count != 0:
                        permissionObjs.delete()
            # 递归复制下一个目录，并制定父目录
                self.getsubobj(id, subfolderObj, flag)

    @method_decorator(httpresult)
    def post(self, request):
        srcFolderId = request.POST.get('srcFolderId')
        dstFolderId = request.POST.get('dstFolderId')

        # test
        username = 'sgf'
        user = User.objects.filter(username=username).first()

        if srcFolderId is None:
            raise ParamMissingException(data=u'缺少源文件夹id')
        if dstFolderId is None:
            raise ParamMissingException(data=u'缺少目的文件夹id')

        srcFolder = Folder.objects.filter(id=srcFolderId).first()
        dstFolder = Folder.objects.filter(id=dstFolderId).first()

        # 文件夹移动
        # 得到url
        src_url = srcFolder.url
        print('src_url:', src_url)
        dst_url = dstFolder.url
        print('dst_url:', dst_url)

        try:
            # 如果将‘与我分享的’的文件夹移动到用户自己目录下，将删除权限
            flag = False
            if srcFolder.creater != dstFolder.creater:
                permissionObjs = Permission.objects.filter(folder=srcFolder)
                if permissionObjs.count != 0:
                    permissionObjs.delete()
                flag = True
            # depth
            depth = dstFolder.depth + 1
            # 数据库操作，修改文件夹父目录
            srcFolder.depth = depth
            srcFolder.parent = dstFolder
            srcFolder.creater = dstFolder.creater
            srcFolder.url = os.path.join(dstFolder.url, srcFolder.name)
            srcFolder.save()

            # 将该目录下的所有对象修改
            self.getsubobj(srcFolderId, srcFolder, flag)
            print('over')
        except Exception:
            raise Exception(u'文件夹剪切失败')
        data = {
            'folderId': srcFolder.id,
            'folderName': srcFolder.name,
            'depth': srcFolder.depth,
        }
        return res(action='CutPaste Folder', data=data)
