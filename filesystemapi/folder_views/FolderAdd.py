from django.views.generic import View
from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator

from filesystemapi.folder_views.function.edit_create import EditCreate

class FolderAdd(View):
    """
        @apiName FolderAdd
        @api {POST} /filesystem/folderadd/
        @apiGroup folder
        @apiVersion 0.0.1
        @apiDescription 新建文件夹
        @apiParam {String} folderName 文件夹名，也可能是新文件夹url
        @apiParam {int} pfolder 父目录id
        @apiParamExample {form-data} 请求样例：
        {
            "pfolder":2,
            "folderName":"hello"
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data 文件夹信息
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Add Folder",
            "data":{
            "folderId": 2,
            "folderName": "helloworld",
            "depth": 1,
            }
        }
    """
    @method_decorator(httpresult)
    def post(self, request):
        pfolderId = request.POST.get('pfolderId')
        folderName = request.POST.get('folderName')
        # test
        username = 'sgf'
        data = EditCreate.folderadd(self, pfolderId, folderName, username)

        return res(action='Add Folder', data=data)





