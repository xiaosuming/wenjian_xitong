from django.views.generic import View
from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator
import json

from filesystemapi.folder_views.function.edit_update import EditUpdate

class FolderRename(View, EditUpdate):
    """
            @apiName FolderRename
            @api {PUT} /filesystem/folderrename/
            @apiGroup folder
            @apiVersion 0.0.1
            @apiDescription 重命名文件夹
            @apiParam {String} folderName 文件夹名
            @apiParam {int} folderId 文件夹id
            @apiParamExample {json} 请求样例：
            {
                "folderId":2,
                "folderName":"hello"
            }
            @apiSuccess (200) {String} msg 信息
            @apiSuccess (200) {String} code 0代表无错误 1代表有错误
            @apiSuccess (200) {json} data 文件夹信息
            @apiSuccessExample {json} 返回样例:
            {
                "code": 0,
                "msg": "SUCCESS",
                "action": "Rename Folder"
                "data":{
                "folderId": 2,
                "folderName": "hello",
            }
    """

    @method_decorator(httpresult)
    def put(self, request):
        data = json.loads(request.body)
        folderId = data.get('folderId')

        foldername = data.get('folderName')
        username = 'sgf'
        data = EditUpdate.folderrename(self, folderId, foldername, username)
        return res(action='Rename Folder', data=data)





