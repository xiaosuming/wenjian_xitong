from django.views.generic import View
from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator

from filesystemapi.folder_views.function.edit_delete import EditDelete


class FolderDelete(View):
    """
        @apiName FolderDel
        @api {DELETE} /filesystem/folderdel/
        @apiGroup folder
        @apiVersion 0.0.1
        @apiDescription 删除文件夹
        @apiParam {int} folderId 文件夹id
        @apiParamExample {params} 请求样例：
        {
            "folderId":1
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data 用户列表
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Delete Folder",
            "data": {
                "folderId": 1,
                "folderName": "hello",
            }
        }
    """
    @method_decorator(httpresult)
    def delete(self, request):
        folderId = request.GET.get('folderId')
        # test
        username = 'sgf'
        data = EditDelete.folderdelete(self, folderId, username)
        return res(action='Delete Folder', data=data)





