from django.views.generic import View
from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator

from filesystemapi.check.file import FileCheck


class FolderCheckAdd(View):
    """
        @apiName FolderCheckAdd
        @api {POST} /filesystem/foldercheckadd/
        @apiGroup check
        @apiVersion 0.0.1
        @apiDescription 新建文件夹时，检查文件夹是否重名
        @apiParam {int} folderId 文件夹id
        @apiParam {string} folderName 文件名
        @apiParamExample {form-data} 请求样例：
        {
            "folderId": 1,
            "folderName": "helloworld",
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Check Add Folder",
            "data": {
                "access": true,
                "type": "normal",
                "duplicate": false
            }
        }
    """


    @method_decorator(httpresult)
    def post(self, request):
        folderId = request.POST.get('folderId')
        newFoldername = request.POST.get('folderName', '')

        # test
        username = 'sgf'

        data = FileCheck.checkadd(self, folderId, newFoldername, username)

        if data['duplicate'] is False:
            return res(action='Check Add Folder', data=data)
        else:
            return res(code=1, msg='ERROR', action='Check Add Folder', data=data)