from files.models.Folder import Folder
from files.models.File import File
from django.views.generic import View
from django.contrib.auth.models import User
from common.ParamMissingException import ParamMissingException
from common.checkpermission import foldercheckpermission
from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator


class FolderCheckPaste(View):
    """
        @apiName FolderCheckPaste
        @api {POST} /filesystem/foldercheckpaste/
        @apiGroup check
        @apiVersion 0.0.1
        @apiDescription 复制及移动时，检查文件夹是否重名有权限操作
        @apiParam {int} srcFolderId 源文件夹id
        @apiParam {int} dstFolderId 目的文件夹id
        @apiParamExample {form-data} 请求样例：
        {
            "srcFolderId": 1,
            "dstFolderId": 2,
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Check Paste Folder",
            "data": {
                "id": "27",
                "right": true,
                "duplicate": false
            }
        }
    """
    @method_decorator(httpresult)
    def post(self, request):
        srcFolderId = request.POST.get('srcFolderId')
        dstFolderId = request.POST.get('dstFolderId')

        # test
        username = 'sgf'
        user = User.objects.filter(username=username).first()

        if srcFolderId is None:
            raise ParamMissingException(u'缺少目录位置信息')
        if dstFolderId is None:
            raise ParamMissingException(u'缺少目录位置信息')

        srcFolder = Folder.objects.filter(id=srcFolderId).first()
        dstFolder = Folder.objects.filter(id=dstFolderId).first()

        if srcFolder is None:
            raise Exception(u'源目录不存在')
        if dstFolder is None:
            raise Exception(u'目的目录不存在')

        data = {
            'id': srcFolderId,
            'right': True,
            'duplicate': False
        }
        # 检查当前用户对源目录是否有写权限
        srcFolderCreater = srcFolder.creater
        if user != srcFolderCreater:
            p = foldercheckpermission(srcFolder, user)
            data['right'] = p

        # 检查当前用户对目的目录是否有写权限
        dstFolderCreater = dstFolder.creater
        if user != dstFolderCreater:
            p = foldercheckpermission(dstFolder, user)
            data['right'] = p
        # 检查是否重名
        folderName = srcFolder.name
        folderObjs = Folder.objects.filter(name=folderName, parent=dstFolder)
        fileObjs = File.objects.filter(name=folderName, pfolder=dstFolder)

        if folderObjs.count() != 0 or fileObjs.count() != 0:
            data['duplicate'] = True
        return res(action='Check Paste Folder', data=data)
