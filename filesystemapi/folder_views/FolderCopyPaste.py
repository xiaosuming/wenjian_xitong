from django.views.generic import View
from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator


from filesystemapi.folder_views.function.edit_update import EditUpdate

class FolderCopyPaste(View, EditUpdate):
    """
          @apiName FolderCopyPaste
          @api {POST} /filesystem/foldercopypaste/
          @apiGroup folder
          @apiVersion 0.0.1
          @apiDescription 文件夹复制粘贴
          @apiParam {int} srcFolderId 源文件夹id
          @apiParam {int} dstFolderId 目的文件夹id
          @apiParamExample {form-data} 请求样例：
          {
              "srcFolderId": 2,
              "dstFolderId": 3
          }
          @apiSuccess (200) {String} msg 信息
          @apiSuccess (200) {String} code 0代表无错误 1代表有错误
          @apiSuccess (200) {json} data
          @apiSuccessExample {json} 返回样例:
          {
              "code": 0,
              "msg": "SUCCESS",
              "action": "CopyPaste Folder",
              "data": {
                "folderId": 2,
                "folderName": "hello",
                "depth": 3,
              }
          }
      """


    @method_decorator(httpresult)
    def post(self, request):
        srcFolderId = request.POST.get('srcFolderId')
        dstFolderId = request.POST.get('dstFolderId')

        # test
        username = 'sgf'
        data = EditUpdate.foldercopy(self, srcFolderId, dstFolderId, username)
        return res(action='CopyPaste Folder', data=data)
