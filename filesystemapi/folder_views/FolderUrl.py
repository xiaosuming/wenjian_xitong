from django.views.generic import View
from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator

from filesystemapi.folder_views.function.edit_search import EditSearch

class FoldersUrl(View):
    """
        @apiName FolderUrl
        @api {GET} /filesystem/folerurl/
        @apiGroup folder
        @apiVersion 0.0.1
        @apiDescription 获取当前用户所有文件夹url
        @apiParamExample {params} 请求样例：
        {
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data url
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Get FolderUrl",
            "data": [
                {
                    "folderId": 1,
                    "folderUrl": "sgf"
                },
                {
                    "folderId": 2,
                    "folderUrl": "sgf/图片"
                },
                {
                    "folderId": 3,
                    "folderUrl": "sgf/视频"
                },
                {
                    "folderId": 4,
                    "folderUrl": "sgf/音频"
                },
                {
                    "folderId": 5,
                    "folderUrl": "sgf/其他"
                },
                {
                    "folderId": 6,
                    "folderUrl": "sgf/与我共享的"
                },
                {
                    "folderId": 7,
                    "folderUrl": "sgf/视频/测试"
                },
                {
                    "folderId": 14,
                    "folderUrl": "sgf/hello"
                }
            ]
        }
    """
    @method_decorator(httpresult)
    def get(self, request):
        # test
        username = 'sgf'
        data = EditSearch.folderurl(self, username)
        return res(action='Get FolderUrl', data=data)
