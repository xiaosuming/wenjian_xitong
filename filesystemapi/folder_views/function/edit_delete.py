from files.models.File import File
from files.models.Folder import Folder
from django.contrib.auth.models import User
from common.checkpermission import foldercheckpermission
from common.res import res

class EditDelete(object):

    def folderdelete(self, folderId, username):
        user = User.objects.filter(username=username).first()

        folder = Folder.objects.filter(id=folderId).first()
        if folder is None:
            raise Exception(u'文件夹不存在')

        # 检查当前用户对此目录是否有写权限
        folderCreater = folder.creater
        if user != folderCreater:
            p = foldercheckpermission(folder, user)
            if p is not True:
                return res(code=1, msg="ERROR", action="Delete Folder", data=u'此用户没有权限')

        try:
            print('开始删除')
            # 递归删除文件
            getsubobj(folderId=folder.id)
            folder.delete()
        except Exception:
            raise Exception(u'文件夹删除失败')
        data = {
            'folderId': folder.id,
            'folderName': folder.name,
        }
        return data
from filesystemapi.file_views.function.edit_delete import EditDelete as Ed
def getsubobj(folderId, self=None):
    # 只删除文件
    print('进入递归函数')
    folder = Folder.objects.filter(id=folderId).first()

    if folder == None:
        return
    fileObj = File.objects.filter(pfolder_id=folder.id)
    # 没有文件进入下一层
    if fileObj.count() == 0:
        getsubobj(folderId=folder.parent_id)
    file_id = []
    for item in fileObj:
        file_id.append(item.id)
    username = 'sgf'
    Ed.filedelete(self, filesId=file_id, username=username)
    getsubobj(folderId=folder.parent_id)


