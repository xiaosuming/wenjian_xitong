from files.models.Folder import Folder
from django.contrib.auth.models import User
from files.models.File import File

from common.ParamMissingException import ParamMissingException
from common.getpath import getpath
from common.folderdel import folderdel

from django.db.models import Q
import shutil
import os

class EditUpdate(object):

    # 文件夹复制的递归函数
    def getsubobj(self, folderId, pfolder):
        print('进入递归函数')
        folder = Folder.objects.filter(id=folderId).first()
        if folder == None:
            return

        subfolderObjs = Folder.objects.filter(Q(parent=folder) & ~Q(name=folder.name))
        subfileObjs = File.objects.filter(pfolder=folder)

        print('subfile_count', subfileObjs.count())
        print('subfolder_count', subfolderObjs.count())
        if subfolderObjs.count() == 0 and subfileObjs.count() == 0:
            return
        if subfileObjs.count() != 0:
            for subfileObj in subfileObjs:
                print('file:', subfileObj)
                subfileObj.id = None
                subfileObj.pfolder = pfolder
                subfileObj.creater = pfolder.creater
                subfileObj.url = os.path.join(pfolder.url, subfileObj.name)
                subfileObj.depth = pfolder.depth + 1
                subfileObj.save()

        if subfolderObjs.count() != 0:
            # 复制该目录下的目录
            for subfolderObj in subfolderObjs:
                print('folder:', subfolderObj)
                id = subfolderObj.id
                subfolderObj.id = None
                subfolderObj.parent = pfolder
                subfolderObj.creater = pfolder.creater
                subfolderObj.url = os.path.join(pfolder.url, subfolderObj.name)
                subfolderObj.depth = pfolder.depth + 1
                subfolderObj.save()
            # 递归复制下一个目录，并制定父目录
                self.getsubobj(id, subfolderObj)
    # 文件夹复制
    def foldercopy(self, srcFolderId, dstFolderId, username):
        user = User.objects.filter(username=username).first()

        if srcFolderId is None:
            raise ParamMissingException(data=u'缺少源文件夹id')
        if dstFolderId is None:
            raise ParamMissingException(data=u'缺少目的文件夹id')

        srcFolder = Folder.objects.filter(id=srcFolderId).first()
        dstFolder = Folder.objects.filter(id=dstFolderId).first()

        if srcFolder is None:
            raise Exception(u'源目录不存在')
        if dstFolder is None:
            raise Exception(u'目的目录不存在')

        # 文件夹复制粘贴
        # 得到url
        src_url = srcFolder.url
        print('src_url:', src_url)
        dst_url = dstFolder.url
        print('dst_url:', dst_url)

        srcFolder = Folder.objects.filter(id=srcFolderId).first()
        dstFolder = Folder.objects.filter(id=dstFolderId).first()

        try:
            # depth
            depth = dstFolder.depth + 1
            print(depth)
            # 数据库操作，复制修改父目录
            srcFolder.id = None
            srcFolder.depth = depth
            srcFolder.parent = dstFolder
            srcFolder.creater = dstFolder.creater
            srcFolder.url = os.path.join(dstFolder.url, srcFolder.name)
            srcFolder.save()
            # 将该目录下的所有对象复制
            self.getsubobj(srcFolderId, srcFolder)
        except Exception:
            raise Exception(u'文件夹复制失败')
            # return res(code=1, msg='ERROR', action='CopyPaste Folder')

        data = {
            'folderId': srcFolder.id,
            'folderName': srcFolder.name,
            'depth': srcFolder.depth,
        }
        return data

    # 文件夹重命名递归函数
    def getsubobjj(self, folder):
        print('进入递归函数')
        if folder == None:
            return

        subfolderObjs = Folder.objects.filter(parent=folder)
        subfileObjs = File.objects.filter(pfolder=folder)

        print('subfile_count', subfileObjs.count())
        print('subfolder_count', subfolderObjs.count())
        if subfolderObjs.count() == 0 and subfileObjs.count() == 0:
            return
        if subfileObjs.count() != 0:
            for subfileObj in subfileObjs:
                subfileObj.url = os.path.join(folder.url, subfileObj.name)
                subfileObj.save()

        if subfolderObjs.count() != 0:
            # 复制该目录下的目录
            for subfolderObj in subfolderObjs:
                subfolderObj.url = os.path.join(folder.url, subfolderObj.name)
                subfolderObj.save()
            # 递归复制下一个目录，并制定父目录
                self.getsubobjj(subfolderObj)


    # 文件夹重命名
    def folderrename(self, folderId, foldername, username):
        print('folderId', folderId)
        print('folderName', foldername)
        # test
        user = User.objects.filter(username=username).first()

        folderObj = Folder.objects.filter(id=folderId).first()
        pfolder = folderObj.parent

        # 得到url
        url = pfolder.url

        try:
            # 在数据库中修改记录
            folderObj.name = foldername
            folderObj.url = os.path.join(url, foldername)
            folderObj.save()
            # 递归修改子目录的url
            self.getsubobjj(folderObj)
            # 修改文件名
        except Exception:
            raise Exception(u'文件夹重命名失败')
        data = {
            'folderId': folderObj.id,
            'folderName': folderObj.name,
        }
        return data