import os
import zipfile

from files.models.File import File
from files.models.Folder import Folder
from django.contrib.auth.models import User
from common.checkpermission import foldercheckpermission
from common.res import res
from common.folderdel import folderdel
from filesystem.settings import URL_BASE_PATH, CLIENT

# 文件夹下载
def folderdownload(folderId, username):
    user = User.objects.filter(username=username).first()

    folderObj = Folder.objects.filter(id=folderId).first()
    if folderObj is None:
        raise Exception(u'目录不存在')

    # 检查当前用户对此目录是否有权限
    folderCreater = folderObj.creater
    if user != folderCreater:
        p = foldercheckpermission(folderObj, user)
        if p is -1:
            return res(code=1, msg="ERROR", action="Down Load Folder", data=u'此用户没有权限')

    folder_list = []
    print('递归开始')
    folder_list = folder_object(folderObj, folder_list)
    print('递归结束')

    # 用户文件夹
    user_path = os.path.join(URL_BASE_PATH, username)
    # 用户选择的文件夹
    path = user_path + '/' + folderObj.url
    # 创建文件夹
    try:
        if os.path.isdir(user_path) is False:
            os.mkdir(user_path)
        for item in folder_list:
            url = os.path.join(user_path, item.url)
            os.mkdir(path=url)
    except Exception:
        folderdel(path)
        raise Exception(u'创建文件失败')

    # 下载文件
    file_object(folder_list, username)
    # 压缩文件夹
    zip_path = compression(path)
    data = {
        'downloadUrl': zip_path,
    }
    return data


# 把该目录下所有子文件夹id存入列表中
# @method_decorator(httpresult)
def folder_object(folderObj, folder_list):
    print('folderObj', folderObj)
    folder_list.append(folderObj)
    id = folderObj.id
    folderObjs = Folder.objects.filter(parent_id=id)
    # 遍历子文件夹
    for item in folderObjs:
        folder_list.append(item)
        foldersObjs = Folder.objects.filter(parent_id=item.id).first()
        # 有子文件夹再次进入递归
        if foldersObjs:
            folder_object(foldersObjs, folder_list)

    return folder_list

# 遍历所有文件
def file_object(folder_list, username):
    objects = []
    for item in folder_list:
        fileObjs = File.objects.filter(pfolder_id=item.id)
        if fileObjs:
            for file in fileObjs:
                objects.append(file)
    file_download(objects, username)

# 下载文件
def file_download(objects, username):
    file_list = []
    for item in objects:
        user_path = os.path.join(URL_BASE_PATH, username, item.url)
        file = [item.hdfsName, user_path]
        file_list.append(file)

    try:
        for item in file_list:
            CLIENT.download(hdfs_path=item[0], local_path=item[1])
    except Exception:
        raise Exception('下载到本地失败')

# 压缩文件
def compression(path):
    try:
        # 压缩文件夹
        zipfile_name = URL_BASE_PATH + '/sgf/' + os.path.split(path)[1] + '.zip'
        file_list = []
        if os.path.isfile(path):
            file_list.append(path)
        else :
            for root, dirs, files in os.walk(path,topdown=False):
                if not files and not dirs:
                    file_list.append(root)
                for name in files:
                    file_list.append(os.path.join(root, name))
        zf = zipfile.ZipFile(zipfile_name, "w", zipfile.zlib.DEFLATED)
        for tar in file_list:
            arcname = tar[len(path):]
            zf.write(tar,arcname)
        zf.close()
        folderdel(path)
        print('压缩成功')
    except Exception:
        folderdel(path)
        raise Exception('文件压缩失败')
    return zipfile_name