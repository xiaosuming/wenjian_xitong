from files.models.Folder import Folder
from django.contrib.auth.models import User
class EditCreate(object):

    # 文件添加
    def folderadd(self, pfolderId, folderName, username):
        user = User.objects.filter(username=username).first()

        if '/' in folderName:
            folderName = folderName.split('/')[-1]

        pfolder = Folder.objects.filter(id=pfolderId).first()
        if pfolder is None:
            raise Exception(u'目录不存在')

        # 得到父目录url
        url = pfolder.url
        print('url:', url)
        # 得到depth
        depth = pfolder.depth + 1

        if folderName is None:
            folderName = u'新建文件夹'

        try:
            # 在数据库中添加记录
            folder = Folder.objects.create(name=folderName, parent=pfolder, depth=depth, creater=pfolder.creater,
                                           url=pfolder.url + '/' + folderName)
            if folder is None:
                raise Exception(u'数据库添加失败')

        except Exception:
            raise Exception(u'添加数据库失败')

        data = {
            'folderId': folder.id,
            'folderName': folder.name,
            'depth': depth,
        }

        return data

