from common.getpath import getpath
from files.models.Folder import Folder
from files.models.File import File
from django.contrib.auth.models import User
from django.db.models import Q
from itertools import chain
from common.startend import startEnd
from common.getson import getson
from common.checkpermission import foldercheckpermission
from common.ParamMissingException import ParamMissingException
from common.getfilesize import getfilesize

import os
import math

class EditSearch(object):

    # 文件夹路径
    def folderurl(self, username):
        print(username)
        user = User.objects.filter(username=username).first()
        print('user:', user)
        # 获取所有的文件夹
        data = []
        folders = Folder.objects.filter(creater=user)
        for folderObj in folders:
            folderObj_info = {
                'folderId': folderObj.id,
                'folderUrl': folderObj.url
            }
            data.append(folderObj_info)
        return data


    # 搜索文件夹及文件（名字）
    def foldersearch(self, folderObj, keyword):
        fileObjs = File.objects.filter(Q(url__icontains=folderObj.name),
                                       Q(name__icontains=keyword))
        folderObjs = Folder.objects.filter(Q(url__icontains=folderObj.name),
                                           Q(name__icontains=keyword))
        return fileObjs, folderObjs

    # 搜索文件夹及文件（名字）
    def searchcommon(self, folderId, keyword, pagesize, page):
        if keyword is None:
            raise ParamMissingException(data=u'缺少关键字')
        if folderId is None:
            raise ParamMissingException(data=u'缺少父目录id')

        folderObj = Folder.objects.filter(id=folderId).first()
        if folderObj is None:
            raise Exception(u'父目录不存在')

        # test
        username = 'sgf'
        user = User.objects.filter(username=username).first()

        # 检查当前用户对此目录是否有权限
        folderCreater = folderObj.creater
        if user != folderCreater:
            p = foldercheckpermission(folderObj, user)
            if p is -1:
                return res(code=1, msg="ERROR", action="Search Common", data=u'此用户没有权限')
        # fileObjs = []
        # folderObjs = []
        if folderObj.depth == 1 and folderObj.name == u'与我分享的':
            fileObjs = File.objects.filter(name__icontains=keyword, permissionFile__user=user)
            if keyword == '':
                # 关键字为空，返回所有文件
                folderObjs = []
            else:
                # #与我分享的，下一级搜索结果
                # 每个文件夹的搜索
                folderObjs = Folder.objects.filter(name__icontains=keyword, permissionFolder__user=user)
                for folderObj in folderObjs:
                    fileObjs1, folderObjs1 = self.foldersearch(folderObj, keyword)
                    fileObjs = chain(fileObjs, fileObjs1)
                    folderObjs = chain(folderObjs, folderObjs1)
        else:
            if keyword == '':
                # 关键字为空，返回所有文件
                fileObjs = File.objects.filter(Q(pfolder=folderObj),
                                               Q(name__icontains=keyword))
                folderObjs = []
            else:
                fileObjs = File.objects.filter(Q(url__icontains=folderObj.name),
                                               Q(name__icontains=keyword))
                folderObjs = Folder.objects.filter(Q(url__icontains=folderObj.name),
                                           Q(name__icontains=keyword))
        # 删除重复元素
        fileObjs = list(set(fileObjs))
        folderObjs = list(set(folderObjs))

        data = {}
        objs = []
        for file in fileObjs:
            size = getfilesize(file.size)
            fileInfo = {
                'fileName': file.name,
                'fileId': file.id,
                'dateCreated': file.createtime,
                'dateModified': file.modifytime,
                'fileSize': size,
                'filePath': file.url,
                'fileType': file.filetype,
                'isFolder': False
            }
            objs.append(fileInfo)
        for folder in folderObjs:
            folderInfo = {
                'fileName': folder.name,
                'fileId': folder.id,
                'dateCreated': folder.createtime,
                'dateModified': None,
                'fileSize': None,
                'filePath': folder.url,
                'fileType': None,
                'isFolder': True
            }
            objs.append(folderInfo)
        print(objs)
        count = len(objs)
        start, end = startEnd(page, count, pagesize)
        # 分页返回
        data['objs'] = objs[start:end]
        data['isFinish'] = False
        if end == count:
            data['isFinish'] = True
        print(data)
        return data

    # 查找某个文件夹内的
    def startEnd(self, page, filescount, pagesize=5):
        maxPage = math.ceil(filescount / pagesize)
        print('maxPage:', maxPage)
        print('pagesize:', pagesize)
        if int(page) == maxPage:
            end = filescount
            start = (maxPage - 1) * pagesize
        elif int(page) < maxPage:
            end = int(page) * pagesize
            start = end - pagesize
        else:
            start = filescount -pagesize
            end = filescount
        return start, end

#     查找某个文件夹内的
    def folderview(self, folderId, page, pagesize, username):
        user = User.objects.filter(username=username).first()

        if folderId is None:
            raise ParamMissingException(u'缺少目录id')

        folderObj = Folder.objects.filter(id=folderId).first()
        if folderObj is None:
            raise Exception(u'目录不存在')

        files, folders = getson(folderId)
        folder_url = folderObj.url
        print('folder_url:', folder_url)
        depth = folderObj.depth
        data = {}
        sd = []
        fd = []
        for folder in folders:
            folderInfo = {
                'folderName': folder.name,
                'folderId': folder.id,
            }
            sd.append(folderInfo)
        data['folder'] = sd

        for file in files:
            size = getfilesize(file.size)
            url = os.path.join(folder_url, file.name)
            fileInfo = {
                'fileName': file.name,
                'fileId': file.id,
                'dateCreated': file.createtime,
                'dateModified': file.modifytime,
                'fileSize': size,
                'filePath': url,
                'fileType': file.filetype
            }
            fd.append(fileInfo)
        # 分片列表
        fileCount = files.count()
        start, end = self.startEnd(page, fileCount, pagesize)
        print('filecount', fileCount)
        print(start, end)
        data['isFinished'] = False
        if end == fileCount:
            data['isFinished'] = True
        data['file'] = fd[start:end]
        data['url'] = folder_url
        data['depth'] = depth
        return data



#     搜索同级文件夹及文件
    def searchssf(self, username, folder_id):
        if folder_id == '0':
            folder_id = 1

        if username is None or folder_id is None:
            raise ParamMissingException(data=u"用户名不能为空")
        user = User.objects.filter(username=username).first()
        folder = Folder.objects.filter(id=folder_id).first()
        url = folder.url
        # 判断是否有权限
        if user.id != folder.creater_id:
            raise Exception(u"该用户没有访问此文件权限")

        # folder的子文件夹
        sfolder_obj = Folder.objects.filter(parent_id=folder.id)
        # folder的子文件
        file_obj = File.objects.filter(pfolder_id=folder.id)
        data = []
        # 添加子文件夹信息
        folders = []
        folder_list = []
        for item in sfolder_obj:
            path = getpath('media', username, url, item.url)
            folder = {
                'folder_id': item.id,
                'key': item.id,
                'title': item.name,
                'folder_name': item.name,
                'folder_createtime': item.createtime,
                'url': username+'/'+item.url,
                'path': path,
            }
            folders.append(folder)
        data.append(folders)


        # 添加子文件信息
        files = []
        for item in file_obj:
            path = getpath('media', username, item.url)
            # 根据判断输出图片
            file = {
                'file_id': item.id,
                'file_name': item.name,
                'key': item.id,
                'title': item.name,
                'folder_id': item.pfolder_id,
                'file_createtime': item.createtime,
                'file_modifytime': item.modifytime,
                'file_type': item.filetype,
                'path': path,
                'url': username + '/' + item.url
            }
            files.append(file)
        data.append(files)
        return data
