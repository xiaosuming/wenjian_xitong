from django.contrib.auth.models import User
from files.models.Folder import Folder
from common.checkpermission import foldercheckpermission
from common.getpath import getpath
from django.db.models import Q
import os

class GetInfo(object):

    def shareme(self, user, forbids):
        # print('shareme start')
        data = []
        folderObjs = Folder.objects.filter(permissionFolder__user=user)
        for folder in folderObjs:
            # 检查该文件夹是否是某个已分享文件夹的子文件夹
            p = foldercheckpermission(folder.parent, user)
            if p != -1:
                continue
            forbid_info = {
                'folderId': folder.id,
                'folderName': folder.name,
                'isAllForbid': False
            }
            forbids.append(forbid_info)
            folder_info = {
                'title': folder.name,
                'id': folder.id,
                'level': folder.depth,
                'folderCreater': folder.creater.username,
                'folderCreatetime': folder.createtime,
                'fatherID': folder.parent.id,
                'folderParentName': folder.parent.name,
                'open': False,
                'selected': False,
                'disabled': False,
                'key': folder.id,
                'expanded': False,
                'icon': 'folder',
                'url': folder.url,
                'isShare': True
            }
            folder_info['children'] = self.getsubfolder(folder, share=True)
            data.append(folder_info)
            print('shareme', data)
        return data

    def getsubfolder(self, folder, share):
        # print('getsubfolder start')
        if folder == None:
            return []

        data = []
        subfolderObjs = Folder.objects.filter(Q(parent=folder))

        if subfolderObjs.count() != 0:
            for subfolderObj in subfolderObjs:
                subfolderInfo = {
                    'title': subfolderObj.name,
                    'id': subfolderObj.id,
                    'level': subfolderObj.depth,
                    'folderCreater': subfolderObj.creater.username,
                    'folderCreatetime': subfolderObj.createtime,
                    'fatherID': folder.id,
                    'folderParentName': folder.name,
                    'open': False,
                    'selected': False,
                    'disabled': False,
                    'key': subfolderObj.id,
                    'expanded': False,
                    'icon': 'folder',
                    'url': subfolderObj.url,
                    'isShare': share
                }

                subfolderInfo['children'] = self.getsubfolder(subfolderObj, share)
                data.append(subfolderInfo)

        else:
            return []
        return data


    def getfolderinfo(self, username):

        # test
        user = User.objects.filter(username=username).first()
        print('user:', user.username)

        # 禁止列表
        forbid = []
        # 创建用户根目录
        rootfolder, created = Folder.objects.get_or_create(name=u'我的文件', parent=None, depth=0,
                                                           creater=user, url=u'我的文件')
        folder_info = {
            'folderId': rootfolder.id,
            'folderName': rootfolder.name,
            'isAllForbid': False
        }
        forbid.append(folder_info)
        # 图片目录
        # 创建用户默认的四个目录
        # print('创建四个目录')
        imagefolder, created = Folder.objects.get_or_create(name=u'图片', parent=rootfolder, creater=user,
                                                            url=rootfolder.url+u'/图片')
        folder_info = {
            'folderId': imagefolder.id,
            'folderName': imagefolder.name,
            'isAllForbid': False
        }
        forbid.append(folder_info)
        # 视频目录
        videofolder, created = Folder.objects.get_or_create(name=u'视频', parent=rootfolder, creater=user,
                                                            url=rootfolder.url+u'/视频')
        folder_info = {
            'folderId': videofolder.id,
            'folderName': videofolder.name,
            'isAllForbid': False
        }
        forbid.append(folder_info)
        # 音频目录
        audiofolder, created = Folder.objects.get_or_create(name=u'音频', parent=rootfolder, creater=user,
                                                            url=rootfolder.url+u'/音频')

        folder_info = {
            'folderId': audiofolder.id,
            'folderName': audiofolder.name,
            'isAllForbid': False
        }
        forbid.append(folder_info)
        # 其他目录
        otherfolder, created = Folder.objects.get_or_create(name=u'其他', parent=rootfolder, creater=user,
                                                            url=rootfolder.url+u'/其他')
        folder_info = {
            'folderId': otherfolder.id,
            'folderName': otherfolder.name,
            'isAllForbid': False
        }
        forbid.append(folder_info)
        # 分享目录
        shareme, created = Folder.objects.get_or_create(name=u'与我分享的', parent=rootfolder, creater=user,
                                                        url=rootfolder.url + u'/与我分享的')
        # print('shareme', shareme)
        folder_info = {
            'folderId': shareme.id,
            'folderName': shareme.name,
            'isAllForbid': True
        }
        forbid.append(folder_info)
        # cvat目录
        cvat, created = Folder.objects.get_or_create(name=u'cvat', parent=rootfolder, creater=user,
                                                     url=rootfolder.url + u'/cvat')
        folder_info = {
            'folderId': cvat.id,
            'folderName': cvat.name,
            'isAllForbid': True
        }
        forbid.append(folder_info)
        # cvat/data目录
        cvat_data, created = Folder.objects.get_or_create(name=u'data', parent=cvat, creater=user,
                                                          url=rootfolder.url + u'/cvat/data')
        folder_info = {
            'folderId': cvat_data.id,
            'folderName': cvat_data.name,
            'isAllForbid': True
        }
        forbid.append(folder_info)

        # print('forbid', forbid)
        # 用户目录初始化
        media_path = getpath('media')
        if not os.path.exists(media_path):
            os.mkdir(media_path)

        user_path = os.path.join(media_path, str(user.username))
        if not os.path.exists(user_path):
            os.mkdir(user_path)

        data = []
        rootfolderInfo = {
            'title': rootfolder.name,
            'id': rootfolder.id,
            'level': rootfolder.depth,
            'folderCreater': rootfolder.creater.username,
            'folderCreatetime': rootfolder.createtime,
            'fatherID': None,
            'folderParentName': None,
            'open': True,
            'selected': True,
            'disabled': False,
            'key': rootfolder.id,
            'expanded': True,
            'icon': 'folder',
            'url': rootfolder.url,
            'isShare': False,
        }

        rootfolderInfo['children'] = self.getsubfolder(folder=rootfolder, share=False)
        print('rootfolderInfo', rootfolderInfo['children'])
        data.append(rootfolderInfo)
        # 获取分享给我的集合
        subfolderInfo = {
            'title': shareme.name,
            'id': shareme.id,
            'level': shareme.depth,
            'folderCreater': shareme.creater.username,
            'folderCreatetime': shareme.createtime,
            'fatherID': shareme.parent.id,
            'folderParentName': shareme.parent.name,
            'open': False,
            'selected': False,
            'disabled': False,
            'key': shareme.id,
            'expanded': False,
            'icon': 'folder',
            'url': shareme.url,
            'isShare': False,
            'children': []
        }
        # 替换与我共享的chilen
        rootChild = data[0]['children']

        if rootChild:

            index = rootChild.index(subfolderInfo)

            rootChild[index]['isShare'] = True
            rootChild[index]['children'] = self.shareme(user, forbid)
        # print(index)

        # 用户信息
        user_info = {
            'userId': user.id,
            'userName': user.username
        }
        data.append(user_info)
        # 获取禁止操作集合
        forbids = {
            'forbids': forbid
        }
        data.append(forbids)
        # print(data)
        return data