from django.views.generic import View
from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator

from filesystemapi.folder_views.function.getfolderinfo import GetInfo

# 遍历根目录
class GetFolderInfo(View, GetInfo):
    """
            @apiName viewallfolders
            @api {GET} /filesystem/viewallfolders/
            @apiGroup folder
            @apiVersion 0.0.1
            @apiDescription 获取用户所有文件及目录
            @apiParamExample {params} 请求样例：
            {

            }
            @apiSuccess (200) {String} msg 信息
            @apiSuccess (200) {String} code 0代表无错误 1代表有错误
            @apiSuccess (200) {json} data
            @apiSuccessExample {json} 返回样例:
            {
            "code": 0,
            "msg": "SUCCESS",
            "action": "ViewAll Folders",
            "data": [
            {
            "title": "sgf",
            "id": 1,
            "level": 0,
            "folderCreater": "sgf",
            "folderCreatetime": "2019-08-01",
            "fatherID": null,
            "folderParentName": null,
            "open": true,
            "selected": true,
            "disabled": false,
            "key": 1,
            "expanded": true,
            "icon": "folder",
            "children": [
                {
                    "title": "图片",
                    "id": 2,
                    "level": 1,
                    "folderCreater": "sgf",
                    "folderCreatetime": "2019-08-01",
                    "fatherID": 1,
                    "folderParentName": "sgf",
                    "open": false,
                    "selected": false,
                    "disabled": false,
                    "key": 2,
                    "expanded": false,
                    "icon": "folder",
                    "children": []
                },
                {
                    "title": "音频",
                    "id": 4,
                    "level": 1,
                    "folderCreater": "sgf",
                    "folderCreatetime": "2019-08-01",
                    "fatherID": 1,
                    "folderParentName": "sgf",
                    "open": false,
                    "selected": false,
                    "disabled": false,
                    "key": 4,
                    "expanded": false,
                    "icon": "folder",
                    "children": [
                        {
                            "title": "hello",
                            "id": 8,
                            "level": 2,
                            "folderCreater": "sgf",
                            "folderCreatetime": "2019-08-05",
                            "fatherID": 4,
                            "folderParentName": "音频",
                            "open": false,
                            "selected": false,
                            "disabled": false,
                            "key": 8,
                            "expanded": false,
                            "icon": "folder",
                            "children": []
                        }
                    ]
                },
                {
                    "title": "其他",
                    "id": 5,
                    "level": 1,
                    "folderCreater": "sgf",
                    "folderCreatetime": "2019-08-01",
                    "fatherID": 1,
                    "folderParentName": "sgf",
                    "open": false,
                    "selected": false,
                    "disabled": false,
                    "key": 5,
                    "expanded": false,
                    "icon": "folder",
                    "children": [
                        {
                            "title": "helloworld",
                            "id": 7,
                            "level": 2,
                            "folderCreater": "sgf",
                            "folderCreatetime": "2019-08-02",
                            "fatherID": 5,
                            "folderParentName": "其他",
                            "open": false,
                            "selected": false,
                            "disabled": false,
                            "key": 7,
                            "expanded": false,
                            "icon": "folder",
                            "children": []
                        }
                    ]
                },
                            ...
                        ]
                    }
                ]
            }
    """
    @method_decorator(httpresult)
    def get(self, request):

        # test
        username = 'sgf'
        data = GetInfo.getfolderinfo(self, username)
        return res(action='ViewAll Folders', data=data)





