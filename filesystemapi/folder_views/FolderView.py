from django.views.generic import View
from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator

from filesystemapi.folder_views.function.edit_search import EditSearch


class FolderView(View, EditSearch):
    """
        @apiName FolderView
        @api {GET} /filesystem/folerview/
        @apiGroup folder
        @apiVersion 0.0.1
        @apiDescription 打开文件夹
        @apiParam {int} folderId 文件夹id
        @apiParam {int} page 页码   用于分页显示
        @apiParamExample {json} 请求样例：
        {
            "folderId":2,
            "page":3,
            "pageSize": 5,
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data 文件信息
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "View Folder",
            "data": {
                "folder": [
                    {
                        "folderName": "图片",
                        "folderId": 2
                    },
                    {
                        "folderName": "视频",
                        "folderId": 3
                    },
                    ...
                ],
                "file": [
                    {
                        "fileName": "helloworld",
                        "fileId": 53,
                        "dateCreated": "2019-08-06",
                        "dateModified": "2019-08-08",
                        "fileSize": "11Kb",
                        "filePath": "sgf/helloworld",
                        "fileType": "image/png"
                    },
                    ...
                ],
                "url": "sgf",
                "depth": 0,
                "isFinished": false
            }
        }
    """


    @method_decorator(httpresult)
    def get(self, request):
        folderId = request.GET.get('folderId')
        page = request.GET.get('page', '1')
        pagesize = request.GET.get('pageSize')
        pagesize = int(pagesize)
        # test
        username = 'sgf'

        data = EditSearch.folderview(self, folderId, page, pagesize, username)
        return res(action='View Folder', data=data)


