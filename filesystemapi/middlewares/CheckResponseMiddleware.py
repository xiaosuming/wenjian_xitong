from django.utils.deprecation import MiddlewareMixin
from filesystem.settings import REDIS
import redis
import datetime
import base64
import hashlib
import json
from common.headers import headers

class CheckResponseMiddleware(MiddlewareMixin):

    def process_response(self, request, response):
        if request.user.is_authenticated:
            if response.status_code == 200 and not headers(request).get('HTTPJWT', None):
                # 请求中没有JWTcookie, 设置cookie
                if response['Content-Type'] == "application/json":
                    JWT = self.generate_JWT(request.user.username)
                    body = json.loads(response.content)
                    body["JWT"] = JWT

                    response.content = json.dumps(body)
        return response


    def generate_JWT(self, username, alg="sha256", salt="filesystem"):
        # 生成用户认证Token
        # 头部标记算法
        header = {
            "typ": "JWT",
            "alg": alg,
        }
        # 负载用户信息
        payload = {
            "username": username
        }
        # 编码便于传递
        encodeHeaderStr = base64.b64encode(str(header).encode("utf8")).decode("utf8")
        encodePayloadStr = base64.b64encode(str(payload).encode("utf8")).decode("utf8")
        message = "{}.{}".format(encodeHeaderStr, encodePayloadStr).encode("utf8")
        # 生成
        hash = hashlib.new(alg)
        hash.update(message)
        hash.update(salt.encode("utf8"))
        today = (datetime.date.today().isoformat()).encode("utf8")
        hash.update(today)
        signature = hash.hexdigest()
        JWT = "{}.{}.{}".format(encodeHeaderStr, encodePayloadStr, signature)
        # 存入redis
        conn = redis.Redis(host=REDIS['HOST'], port=REDIS['PORT'], password=REDIS['PASSWORD'], db=1, decode_responses=True)
        conn.set(signature, username)  # 缓存JWT作为局部会话
        conn.expire(signature, 60*60*24*30)  # 设置存储时间为一个月
        return JWT

    def verify_JWT(self, JWT, salt="filesystem"):
        # 解析传入的UAT
        # 解析算法
        encodeHeaderStr, encodePayloadStr, signature = JWT.split(".")
        headerStr = base64.b64decode(encodeHeaderStr.encode("utf8")).decode("utf8")
        header = eval(headerStr)
        alg = header["alg"]
        # 校验签名
        message = "{}.{}".format(encodeHeaderStr, encodePayloadStr).encode("utf8")
        hash = hashlib.new(alg)
        hash.update(message)
        hash.update(salt.encode('utf8'))
        curSignature = hash.hexdigest()
        if curSignature != signature:
            return (False, None)
        # 解析用户信息
        payloadStr = base64.b64decode(encodePayloadStr.encode("utf8")).decode("utf8")
        payload = eval(payloadStr)
        userName = payload["username"]
        return (True, userName)