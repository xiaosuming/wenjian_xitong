from django.conf import settings
from django.contrib.auth.models import User
from django.utils.deprecation import MiddlewareMixin
from .CheckResponseMiddleware import CheckResponseMiddleware
from filesystem.settings import REDIS
from common.headers import headers
import redis


class CheckJWTMiddleware(MiddlewareMixin):
    def process_request(self, request):
        white_url_pattern = settings.WHITE_PATTERNS
        Patterned_URL = []
        url_path = request.get_full_path()
        # 需要跳过的路由过滤
        for pattern in white_url_pattern:
            res = pattern.findall(url_path)
            for i in res:
                Patterned_URL.append(i)
        username = request.GET.get('username')
        if len(Patterned_URL) > 0 and username:
            # 这些类似于静态路由, 但也是由后台控制,所以对用户不需要严格认证.指定一个默认用户用于跳过验证
            print('跳过登录')
            request.user = User.objects.get(username=username)
            return

        request_headers = headers(request)
        # 检查请求头中是否有JWT
        JWT = request_headers.get("JWT", None)
        if JWT:
            # 有局部会话，检查缓存中的局部会话信息
            conn = redis.Redis(host=REDIS["HOST"], port=REDIS["PORT"], password=REDIS["PASSWORD"], db=1, decode_responses=True)
            signature = JWT.split(".")[2]
            username = conn.get(signature)
            if username:
                # 缓存中有未过期的局部会话, 为当前请求添加用户
                userObj = User.objects.get(username=username)
                request.user = userObj
            else:
                # 缓存中没有局部会话，删除请求中的JWT

                # 校验JWT
                JWTCheckor = CheckResponseMiddleware()
                verify, userName = JWTCheckor.verify_JWT(JWT)
                if verify:
                    # 校验成功, 为当前请求添加用户
                    userObj = User.objects.get(username=userName)
                    request.user = userObj