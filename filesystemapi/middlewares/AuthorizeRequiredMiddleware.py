# -*- coding:utf-8 -*-
from django.contrib.auth.models import User
from django.utils.deprecation import MiddlewareMixin
from django.http import JsonResponse
from filesystem.settings import OAUTH_AUTHORIZE_URI, verifyUATURI
from common.headers import headers
import requests


class AuthorizeRequiredMiddleware(MiddlewareMixin):
    def process_request(self, request):
        request_headers = headers(request)
        if not request.user.is_authenticated:
            # 用户首次访问，检查全局会话UAT
            UAT = request_headers.get("HTTP_UAT", None)
            if UAT:  # 有全局会话，请求认证服务器
                responseData = requests.get(url=verifyUATURI, headers={"UAT": UAT})
                resJson = responseData.json()
                verified = resJson['data']['verified']
                if verified:  # 全局会话合法
                    userName = resJson['data']['username']
                    try:
                        userObj = User.objects.get(username=userName)
                    except Exception:
                        userObj = User.objects.create(username=userName, password="guangji2019")
                    # 为当前请求添加用户实例
                    request.user = userObj
                    return

            #没有全局会话，重定向至logincheck
            res = {
                "code": 1,
                "msg": "无权限",
                "data": {
                    "location": OAUTH_AUTHORIZE_URI,
                }
            }
            return JsonResponse(res)

