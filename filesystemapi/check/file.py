from files.models.Folder import Folder
from files.models.File import File
from django.contrib.auth.models import User
from common.ParamMissingException import ParamMissingException
from common.checkpermission import foldercheckpermission, onefilecheckpermission

from common.res import res

class FileCheck(object):

    # 检查文件添加
    def checkadd(self, folderId, filename, username):

        user = User.objects.filter(username=username).first()
        if folderId is None:
            raise ParamMissingException(u'缺少目录位置信息')
        if filename is None:
            raise ParamMissingException(u'缺少新建目录名')

        # 检查权限
        folderObj = Folder.objects.filter(id=folderId).first()
        if folderObj is None:
            raise Exception(u'目录不存在')
        data = {}
        # 检查当前用户对此目录是否有写权限
        folderCreater = folderObj.creater
        if user != folderCreater:
            p = foldercheckpermission(folderObj, user)
            if p is not True:
                data['access'] = False
                return res(code=1, msg="ERROR", action="Check Add file", data=data)
        data['access'] = True

        # 检查重名
        folderObjs = Folder.objects.filter(name=filename, parent=folderObj)
        fileObjs = File.objects.filter(name=filename, pfolder=folderObj)

        if folderObjs.count() == 0 and fileObjs.count() == 0:
            data['duplicate'] = False
            return data
        else:
            data['duplicate'] = True
            return data

    # 检查文件删除
    def checkdel(self, filesId, username):
        user = User.objects.filter(username=username).first()

        if filesId is None:
            raise ParamMissingException(data=u'缺少文件位置信息')

        filesList = []
        # 转换成列表
        filestemp = eval(filesId)

        # 是一个id
        if type(filestemp) is int:
            filesList.append(filestemp)
        else:
            filesList = list(filestemp)

        idList = []
        # 检查权限
        # 检查当前用户对此目录是否有写权限
        for myId in filesList:
            fileObj = File.objects.filter(id=myId).first()
            fileCreater = fileObj.creater
            if user != fileCreater:
                p = onefilecheckpermission(fileObj, user)
                if p is not True:
                    continue

            idList.append(myId)

        data = {
            'id': idList,
            'count': len(idList)
        }
        return data

    # 检查文件复制粘贴
    def checkpaste(self, filesId, dstFolderId, username):
        user = User.objects.filter(username=username).first()

        if filesId is None:
            raise ParamMissingException(data=u'缺少文件位置信息')

        if dstFolderId is None:
            raise ParamMissingException(data=u'缺少文件位置信息')

        filesList = []
        # 转换成列表
        filestemp = eval(filesId)

        # 是一个id
        if type(filestemp) is int:
            filesList.append(filestemp)
        else:
            filesList = list(filestemp)

        folderObj = Folder.objects.filter(id=dstFolderId).first()
        if folderObj is None:
            raise Exception(u'目录不存在')

        data = []
        for objId in filesList:
            fileObj = File.objects.filter(id=objId).first()
            if fileObj is None:
                raise Exception(u'文件不存在')
            file_info = {
                'id': objId,
                'duplicate': False,
                'right': True
            }
            # 检查权限
            # 检查当前用户对此目录是否有写权限
            fileCreater = fileObj.creater
            if user != fileCreater:
                p = onefilecheckpermission(fileObj, user)
                file_info['right'] = p
            # 检查重名
            name = fileObj.name
            folderObjs = Folder.objects.filter(name=name, parent=folderObj)
            fileObjs = File.objects.filter(name=name, pfolder=folderObj)

            if folderObjs.count() != 0 or fileObjs.count() != 0:
                file_info['duplicate'] = True

            data.append(file_info)
        return data

    # 检查文件剪贴
    # def

    # 检查文件重命名
    def checkrename(self, fileId, fileName, username):
        user = User.objects.filter(username=username).first()

        if fileId is None:
            raise ParamMissingException(data=u'缺少文件位置信息')
        if fileName is None:
            raise ParamMissingException(data=u'缺少新建文件名')

        fileObj = File.objects.filter(id=fileId).first()
        if fileObj is None:
            raise Exception(u'目录不存在')

        data = {}
        # 检查权限
        # 检查当前用户对此目录是否有写权限
        fileCreater = fileObj.creater
        if user != fileCreater:
            p = onefilecheckpermission(fileObj, user)
            if p is not True:
                data['access'] = False
                return res(code=1, msg="ERROR", action="Check Rename File", data=data)
        data['access'] = True

        # 检查重名
        pfolderObj = fileObj.pfolder
        folderObjs = Folder.objects.filter(name=fileName, parent=pfolderObj)
        fileObjs = File.objects.filter(name=fileName, pfolder=pfolderObj)

        if folderObjs.count() == 0 and fileObjs.count() == 0:
            data['duplicate'] = False
            return data
        else:
            data['duplicate'] = True
            return data