from files.models.Folder import Folder
from files.models.File import File
from django.contrib.auth.models import User
from common.ParamMissingException import ParamMissingException
from common.checkpermission import foldercheckpermission
from common.getpath import getpath
from common.res import res
from pathlib import PurePath
import os

class CheckFolder(object):

    def checkurl(self, username, url):
        path = PurePath(url)
        code = 0
        user_path = getpath('media', username)
        if not os.path.exists(os.path.join(user_path, path.parent)):
            code = 1
        string = url.split('/')
        if '' in string:
            code = 1
        if '..' in string:
            code = 1
        return code
    # 检查文件夹添加
    def checkadd(self, folderId, newFoldername, username):
        user = User.objects.filter(username=username).first()

        if folderId is None:
            raise ParamMissingException(u'缺少目录位置信息')
        if newFoldername is None:
            raise ParamMissingException(u'缺少新建目录名')

        # 检查权限
        pfolder = Folder.objects.filter(id=folderId).first()
        if pfolder is None:
            raise Exception(u'目录不存在')

        data = {}
        # 检查当前用户对此目录是否有写权限
        folderCreater = pfolder.creater
        if user != folderCreater:
            p = foldercheckpermission(pfolder, user)
            if p is not True:
                data['access'] = False
                return res(code=1, msg="ERROR", action="Check Add Folder", data=data)
        data['access'] = True

        # 检查重名
        if '/' in newFoldername:
            # 检查url是否合法
            code = self.checkurl(username, newFoldername)
            data['type'] = 'url'
            if code:
                data['path'] = False
                return res(code=1, msg='ERROR', action='Check Add Folder', data=data)
            data['path'] = True
            folderObjs = Folder.objects.filter(url=newFoldername)
            fileObjs = File.objects.filter(url=newFoldername)
        else:
            data['type'] = 'normal'
            folderObjs = Folder.objects.filter(name=newFoldername, parent=pfolder)
            fileObjs = File.objects.filter(name=newFoldername, pfolder=pfolder)

        if folderObjs.count() == 0 and fileObjs.count() == 0:
            data['duplicate'] = False
            return data
        else:
            data['duplicate'] = True
            return data


    # 检查文件夹删除
    # def checkdel(self, ):
