from django.contrib.auth.models import User
from files.models.Permission import Permission
from common.ParamMissingException import ParamMissingException

class Modify(object):

    def modify(self, pId, right, username):
        user = User.objects.filter(username=username).first()

        if pId is None:
            raise ParamMissingException(data=u'缺少权限id')
        right = int(right)
        permissionObj = Permission.objects.filter(id=pId).first()
        if permissionObj is None:
            raise Exception(u'权限不存在')
        print('8' * 8)
        permissionObj.right = right
        permissionObj.save()
        data = {
            'pId': permissionObj.id,
            'user': permissionObj.user.username,
            'userId': permissionObj.user.id,
            'right': permissionObj.right,
        }
        if permissionObj.folder is None:
            data['Id'] = permissionObj.file.id
            data['Name'] = permissionObj.file.name
            data['isFolder'] = False
        else:
            data['Id'] = permissionObj.folder.id
            data['Name'] = permissionObj.folder.name
            data['isFolder'] = True
        return data