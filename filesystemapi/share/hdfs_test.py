from hdfs import *
# 文件操作：上传(不是分片)，删除
# 文件夹操作： 创建

client = InsecureClient("http://192.168.50.166:50070", user="filesystem")
# client = Client("http://192.168.50.166:9000", root='/')
# # # 显示文件夹内的所有文件和文件夹
a = client.list("/")
print(a)
# #  文件或文件夹详细信息
# b = client.status("/test", strict=False)
# c = client.status(hdfs_path='1.jpg')
# d = client.status(hdfs_path='2.jpeg')
# print(b)
# print(c,d)
#
# c = client.set_times(hdfs_path='/home')
# print(c)

# 上传文件
# with open('/home/xintec/Downloads/VOC2007.zip', 'rb') as f:
#     data = f.read()
#     # print(data)
# client.write(hdfs_path='/filesystem/VOC2007.zip', data=data)

# # 读文件
# file = client.read('/home/test.txt', chunk_size=1024, encoding='utf-8')
#
# with file as f:
#     for item in f:
#         print(item)

# # 删除文件
# for item in a:
#     client.delete(hdfs_path=item, recursive=False)
# client.delete(hdfs_path='/Acceptation', recursive=True)


# 重命名文件
# client.rename(hdfs_src_path='a.jpg', hdfs_dst_path='1.jpg')

# 下载文件
# with client.read('VOC2007.zip') as reader:
#     features = reader.read()
#     f = open('/home/xintec/VOC2007.zip', 'wb')
#     f.write(features)
# 创建文件夹
# folder = client.makedirs(hdfs_path='/tests1/testsss')
# print('folder', folder)
