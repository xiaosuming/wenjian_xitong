from django.contrib.auth.models import User
from files.models.Permission import Permission
from common.startend import startEnd
class Manage(object):

    def getright(self, right):
        if right == True:
            p = 'True'
        else:
            p = 'False'
        return p

    def sharemanage(self, pagesize, page, username):
        user = User.objects.filter(username=username).first()

        data = {}
        objs = []
        # 获取文件夹权限
        permissions = Permission.objects.filter(folder__creater=user)
        for p in permissions:
            right = self.getright(p.right)
            permissim_info = {
                'pId': p.id,
                'user': p.user.username,
                'userId': p.user.id,
                'Id': p.folder.id,
                'Name': p.folder.name,
                'right': right,
                'isFolder': True
            }
            objs.append(permissim_info)
        # 获取文件权限
        permissions = Permission.objects.filter(file__creater=user)
        for p in permissions:
            right = self.getright(p.right)
            permissim_info = {
                'pId': p.id,
                'user': p.user.username,
                'userId': p.user.id,
                'Id': p.file.id,
                'Name': p.file.name,
                'right': right,
                'isFolder': False
            }
            objs.append(permissim_info)
        count = len(objs)
        start, end = startEnd(page, count, pagesize)
        # 分页返回
        data['objs'] = objs[start:end]
        data['isFinish'] = False
        if end == count:
            data['isFinish'] = True
        return data