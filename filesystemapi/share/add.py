from files.models.Folder import Folder
from django.contrib.auth.models import User
from files.models.File import File
from files.models.Permission import Permission
from common.ParamMissingException import ParamMissingException
from common.getpath import getpath
import os
class Add(object):

    def shareadd(self, urls, usersId, right, username):
        user = User.objects.filter(username=username).first()

        # url列表
        urlList = []
        urlTemp = eval(urls)
        if type(urlTemp) is str:
            urlList.append(urlTemp)
        else:
            urlList = list(urlTemp)

        # 分享受用者list
        usersIdList = []
        tempList = eval(usersId)

        if type(tempList) is int:
            usersIdList.append(tempList)
        else:
            usersIdList = list(tempList)

        if len(urlList) == 0:
            raise ParamMissingException(data=u'缺少url')
        if len(usersIdList) == 0:
            raise ParamMissingException(data=u'缺少使用者名')

        userObjList = []
        userNameList = []
        for userId in usersIdList:
            userObj = User.objects.filter(id=userId).first()
            if userObj is None:
                raise Exception(u'此用户不存在')
            userNameList.append(userObj.username)
            userObjList.append(userObj)
        url = urlList[0]
        # 用户根目录
        user_path = getpath('media', str(user.username))
        path = os.path.join(user_path, url)
        print(path)
        if not os.path.exists(path):
            raise Exception(u'资源不存在')
        perm_info = {
            'userNames': userNameList,
            'userIds': usersIdList,
            'right': right,
        }
        folderObj = Folder.objects.filter(url=url).first()
        # 是文件夹
        if len(urlList) == 1 and folderObj:
            print('folder')
            for userObj in userObjList:
                permissionObj = Permission.objects.filter(user=userObj, folder=folderObj).first()
                # 若存在就修改， 否则新建
                if permissionObj:
                    permissionObj.right = right
                    permissionObj.save()
                else:
                    newPermissionObj = Permission.objects.create(user=userObj, folder=folderObj, right=right)
            perm_info['folderId'] = folderObj.id
        else:
            print('file')
            fileIdList = []
            for url in urlList:
                fileObj = File.objects.filter(url=url).first()
                if fileObj is None:
                    raise Exception(u'文件不存在')
                fileIdList.append(fileObj.id)
                for userObj in userObjList:
                    permissionObj = Permission.objects.filter(user=userObj, file=fileObj).first()
                    if permissionObj:
                        permissionObj.right = right
                        permissionObj.save()
                    else:
                        newPermissionObj = Permission.objects.create(user=userObj, file=fileObj, right=right)
            perm_info['fileId'] = fileIdList

        return perm_info