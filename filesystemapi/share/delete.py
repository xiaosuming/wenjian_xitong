from django.contrib.auth.models import User
from files.models.Permission import Permission
from common.ParamMissingException import ParamMissingException

class Delete(object):

    def sharedel(self, pId, username):
        user = User.objects.filter(username=username).first()

        if pId is None:
            raise ParamMissingException(data=u'缺少权限id')
        # 获取id列表
        idList = []
        if type(pId) is int:
            idList.append(pId)
        else:
            idList = list(pId)

        # 循环删除权限
        for id in idList:
            permissionObj = Permission.objects.filter(id=id).first()
            if permissionObj is None:
                raise Exception(u'权限不存在')
            permissionObj.delete()