from django.views.generic import View

from sendfile import sendfile

import os

class GetFile(View):
    def get(self, request):
        # path为绝对路径
        path = request.GET.get('path')
        name = os.path.split(path)
        print(name)
        return sendfile(request, filename=path, attachment=True)
