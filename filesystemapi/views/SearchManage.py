from files.models.Permission import Permission
from django.contrib.auth.models import User
from django.views.generic import View
from common.httpresult import httpresult
from common.ParamMissingException import ParamMissingException
from common.startend import startEnd
from common.res import res
from django.utils.decorators import method_decorator
from django.db.models import Q


class SearchManage(View):
    """
        @apiName SearchManage
        @api {GET} /filesystem/searchmanage/
        @apiGroup Search
        @apiVersion 0.0.1
        @apiDescription 搜索文件夹及文件（名字）,在sharemanage中使用
        @apiParam {string} keyWord 搜索关键字
        @apiParam {int} pageSize 页面大小，一页显示多少条数据
        @apiParam {int} page 页码,分页显示排序结果
        @apiParamExample {params} 请求样例：
        {
            "keyWord": "hello",
            "pageSize": 20,
            "page": 1,
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Search",
            "data": {
            "url": "/sgf/其他/其他",
            "depth": 3，
            "isFinished": False,
            'file":
            [
                {
                "fileName": "q1",
                "fileId": 29,
                "dateCreated": "2019-08-02",
                "dateModified": "2019-08-02",
                "fileSize": "0b",
                "filePath": "/sgf/其他/其他",
                "fileType": null，
                },
                ...
            ],
        }
    """
    def getright(self, right):
        if right == True:
            p = 'True'
        else:
            p = 'False'
        return p


    @method_decorator(httpresult)
    def get(self, request):
        keyword = request.GET.get('keyWord')
        page = request.GET.get('page')
        pagesize = request.GET.get('pageSize')
        pagesize = int(pagesize)
        page = int(page)

        if keyword is None:
            raise ParamMissingException(data=u'缺少关键字')

        # test
        username = 'sgf'
        user = User.objects.filter(username=username).first()

        pFileObjs = Permission.objects.filter(Q(file__creater=user, folder=None),
                    Q(file__name__icontains=keyword))
        pFolderObjs = Permission.objects.filter(Q(folder__creater=user, file=None),
                    Q(folder__name__icontains=keyword))
        data = {}
        objs = []
        for pFileObj in pFileObjs:
            file = pFileObj.file
            right = self.getright(pFileObj.right)
            fileInfo = {
                'pId': pFileObj.id,
                'user': pFileObj.user.username,
                'userId': pFileObj.user.id,
                'Id': file.id,
                'Name': file.name,
                'right': right,
                'isFolder': False
            }
            objs.append(fileInfo)
        # fd = sorted(fd, key=lambda keys: keys['filePath'], reverse=False)
        for pFolderObj in pFolderObjs:
            folder = pFolderObj.folder
            right = self.getright(pFolderObj.right)
            folderInfo = {
                'pId': pFolderObj.id,
                'user': pFolderObj.user.username,
                'userId': pFolderObj.user.id,
                'Id': folder.id,
                'Name': folder.name,
                'right': right,
                'isFolder': True
            }
            objs.append(folderInfo)
        # sd = sorted(sd, key=lambda keys: keys['folderPath'], reverse=False)
        count = len(objs)
        start, end = startEnd(page, count, pagesize)
        # 分页返回
        data['objs'] = objs[start:end]
        data['isFinish'] = False
        if end == count:
            data['isFinish'] = True
        return res(action='Search Manage', data=data)
