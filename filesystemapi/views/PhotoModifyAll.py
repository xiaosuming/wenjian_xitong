from files.models.File import File
from django.views.generic import View
from django.contrib.auth.models import User
from common.ParamMissingException import ParamMissingException
from common.photohandle import setxml, setyolo
from common.getxml import getrawxml
from common.httpresult import httpresult
from common.getpath import getpath
from common.res import res
from django.utils.decorators import method_decorator
from PIL import Image
import json
import os
import shutil


class PhotoModifyAll(View):
    """
        @apiName PhotoModifyAll
        @api {PUT} /filesystem/photomodifyall/
        @apiGroup sign
        @apiVersion 0.0.1
        @apiDescription 修改图片，操作顺序：裁剪，旋转，缩放;现在支持xml，yolo格式； 此操作前可先调用filecheckrename进行权限检查
        @apiParam {int} fileId 文件id
        @apiParam {int} left 裁剪框左上角横坐标
        @apiParam {int} top 裁剪框左上角纵坐标
        @apiParam {int} width 新图片宽度
        @apiParam {int} height 新图片高度
        @apiParam {int} angle  旋转角度
        @apiParamExample {json} 请求样例：
        {
            "fileId": 1,
            "left": 20,
            "top": 30,
            "width": 500,
            "height": 600,
            "angle": 90
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Photo Modify",
            "data": {
            }
        }
    """
    # 将yolo比率格式转化成（xmin, ymin, xmax, ymax)
    def getxy(self, sign_path, photoWidth, photoHeight):
        fd_txt = open(sign_path)
        data = []
        for coordinate in fd_txt.readlines():
            mystr = coordinate.split(' ', 4)
            xmin = (float(mystr[1]) - float(mystr[3]) * 0.5) * photoWidth + 1
            ymin = (float(mystr[2]) - float(mystr[4]) * 0.5) * photoHeight + 1
            height = (float(mystr[4]) * photoHeight)
            width = (float(mystr[3]) * photoWidth)
            cubeStyle = {
                'sign': int(mystr[0]),
                'xmin': int(xmin),
                'ymin': int(ymin),
                'xmax': int(xmin) + int(width),
                'ymax': int(ymin) + int(height)
            }
            data.append(cubeStyle)
        return data

    # 裁剪框四元组，标记框数据列表
    def checkphoto(self, cXmin, cYmin, cWidth, cHeight, data):
        print('checkphoto')
        for sign_data in data:
            sXmin = sign_data['xmin']
            sYmin = sign_data['ymin']
            sXmax = sign_data['xmax']
            sYmax = sign_data['ymax']
            if sXmin <= cXmin or sXmax >= cXmin + cWidth or \
                    sYmin <= cYmin or sYmax >= cYmin + cHeight:
                return False
        return True

    @method_decorator(httpresult)
    def put(self, request):
        data = json.loads(request.body)
        print(data)
        fileId = data.get('fileId')
        left = data.get('x')
        top = data.get('y')
        width = data.get('width')
        height = data.get('height')
        angle = data.get('rotate')
        scaleX = data.get('scaleX')
        scaleY = data.get('scaleY')

        # test
        username = 'sgf'
        user = User.objects.filter(username=username).first()

        if fileId is None:
            raise ParamMissingException(data=u'缺少文件位置信息')
        if left is None:
            raise ParamMissingException(data=u'缺少left')
        if top is None:
            raise ParamMissingException(data=u'缺少top')
        if width is None:
            raise ParamMissingException(data=u'缺少width')
        if height is None:
            raise ParamMissingException(data=u'height')
        if angle is None:
            raise ParamMissingException(data=u'angle')

        # #图片及标记文件路径
        fileObjs = File.objects.filter(id=fileId).first()
        fileCreater = fileObjs.creater
        name = fileObjs.name
        code = name.split('.')[0]
        media_path = getpath('media')

        photo_path = os.path.join(media_path, fileCreater.username, fileObjs.url)
        temp_path = getpath('temp', fileCreater.username)

        # sign文件夹路径
        path = os.path.join(media_path, 'sign', code)
        # 检查是否是标记图片
        sign = False
        if os.path.exists(path):
            sign = True
        files = os.listdir(path)
        # 标记文件类型
        type = 'xml'
        sign_file = ''
        if code+'.xml' in files:
            sign_file = code + '.xml'
        elif code+'.txt' in files:
            type = 'yolo'
            sign_file = code + '.txt'
        else:
            raise Exception(u'不支持此标记文件')
        # 标记文件路径
        sign_path = os.path.join(path, sign_file)
        print(sign_path)
        sign_data = ''
        if sign:
            # 获取标记框坐标信息
            if type == 'xml':
                sign_data = getrawxml(sign_path)
            elif type == 'yolo':
                im = Image.open(photo_path)
                width, height = im.size
                sign_data = self.getxy(sign_path, width, height)
            else:
                raise Exception(u'暂不支持此标记格式')
            print(sign_data)
            # 检查修改图片时，检查裁剪是否将标记框剪碎
            status = self.checkphoto(int(left), int(top), int(width), int(height), sign_data)
            print('status', status)
            if not status:
                raise Exception(u'此裁剪已破坏标记框')

        # 保存文件副本
        temp_photo_path = os.path.join(temp_path, fileObjs.name)
        temp_sign_path = os.path.join(temp_path, sign_file)
        shutil.copy(photo_path, temp_photo_path)
        shutil.copy(sign_path, temp_sign_path)
        try:
            # 裁剪图片参数（xmin, ymin, xmax, ymax)
            print('裁剪图片')
            im = Image.open(photo_path)
            xmin = int(left)
            ymin = int(top)
            xmax = int(left) + int(width)
            ymax = int(top) + int(height)
            crop = im.crop((xmin, ymin, xmax, ymax))
            crop.save(photo_path)

            # # 旋转图片
            im = Image.open(photo_path)
            print(im.size)
            # 此角度是顺时针，而Image是逆时针
            angle = angle % 360
            print(angle)
            if angle == 0:
                pass
            else:
                print('旋转')
                if angle == 90:
                    out = im.transpose(Image.ROTATE_270)
                elif angle == 180:
                    out = im.transpose(Image.ROTATE_180)
                elif angle == 270:
                    print(270)
                    out = im.transpose(Image.ROTATE_90)
                else:
                    raise Exception(u'不支持此旋转')
                out.save(photo_path)

            # 图片缩放
            print('图片缩放')
            im = Image.open(photo_path)
            photoWidth, photoHeight = im.size
            x = int(photoWidth*scaleX)
            y = int(photoHeight*scaleY)
            out = im.resize((x, y))
            out.save(photo_path)

            # 修改对应xml文件 或yolo文件或者其他标记文件
            if sign:
                if type == 'xml':
                    setxml(photo_path, sign_path, left, top, width, height, scaleX, scaleY, angle)
                elif type == 'yolo':
                    setyolo(photo_path, sign_path, sign_data, left, top, width, height, scaleX, scaleY, angle)
                else:
                    raise Exception(u'暂不支持此标记格式')
        except Exception:
            shutil.move(temp_photo_path, photo_path)
            shutil.move(temp_sign_path, sign_path)
            return res(code=1, msg='ERROR', action='Photo Modify')
        # 删除备份文件
        os.remove(temp_photo_path)
        os.remove(temp_sign_path)
        return res(action='Photo Modify')
