from files.models.File import File
from django.views.generic import View
from django.contrib.auth.models import User
from common.ParamMissingException import ParamMissingException
from common.photohandle import setyolo
from common.httpresult import httpresult
from common.getpath import getpath
from common.res import res
from django.utils.decorators import method_decorator
from PIL import Image
import json
import os
import shutil


class PhotoModify2(View):
    """
        @apiName PhotoModify
        @api {PUT} /filesystem/photomodify2/
        @apiGroup sign
        @apiVersion 0.0.1
        @apiDescription 修改图片，操作顺序：裁剪，旋转，缩放；仅yolo支持格式，此接口可以不用
        @apiParam {int} fileId 文件id
        @apiParam {int} left 裁剪框左上角横坐标
        @apiParam {int} top 裁剪框左上角纵坐标
        @apiParam {int} width 新图片宽度
        @apiParam {int} height 新图片高度
        @apiParam {int} angle  旋转角度
        @apiParamExample {json} 请求样例：
        {
            "fileId": 1,
            "left": 20,
            "top": 30,
            "width": 500,
            "height": 600,
            "angle": 90
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Photo Modify",
            "data": {
            }
        }
    """
    # 将比率格式转化成（xmin, ymin, xmax, ymax)
    def getxy(self, txt_path, photoWidth, photoHeight):
        fd_txt = open(txt_path)
        data = []
        for coordinate in fd_txt.readlines():
            mystr = coordinate.split(' ', 4)
            xmin = (float(mystr[1]) - float(mystr[3]) * 0.5) * photoWidth + 1
            ymin = (float(mystr[2]) - float(mystr[4]) * 0.5) * photoHeight + 1
            height = (float(mystr[4]) * photoHeight)
            width = (float(mystr[3]) * photoWidth)
            cubeStyle = {
                'sign': int(mystr[0]),
                'xmin': int(xmin),
                'ymin': int(ymin),
                'xmax': int(xmin) + int(width),
                'ymax': int(ymin) + int(height)
            }
            data.append(cubeStyle)
        return data

    # 裁剪框四元组，标记框数据列表
    def checkphoto(self, cXmin, cYmin, cWidth, cHeight, data):
        print('checkphoto')
        for sign_data in data:
            sXmin = sign_data['xmin']
            sYmin = sign_data['ymin']
            sXmax = sign_data['xmax']
            sYmax = sign_data['ymax']
            if sXmin <= cXmin or sXmax >= cXmin+cWidth or \
                    sYmin <= cYmin or sYmax >= cYmin+cHeight:
                return False
        return True

    @method_decorator(httpresult)
    def put(self, request):
        data = json.loads(request.body)
        print(data)
        fileId = data.get('fileId')
        left = data.get('x')
        top = data.get('y')
        width = data.get('width')
        height = data.get('height')
        angle = data.get('rotate')
        scaleX = data.get('scaleX')
        scaleY = data.get('scaleY')

        # test
        username = 'sgf'
        user = User.objects.filter(username=username).first()

        if fileId is None:
            raise ParamMissingException(data=u'缺少文件位置信息')
        if left is None:
            raise ParamMissingException(data=u'缺少left')
        if top is None:
            raise ParamMissingException(data=u'缺少top')
        if width is None:
            raise ParamMissingException(data=u'缺少width')
        if height is None:
            raise ParamMissingException(data=u'height')
        if angle is None:
            raise ParamMissingException(data=u'angle')

        # 修改图片
        fileObjs = File.objects.filter(id=fileId).first()
        fileCreater = fileObjs.creater
        media_path = getpath('media')
        file_path = os.path.join(media_path, fileCreater.username, fileObjs.url)
        temp_path = getpath('temp', fileCreater.username)

        # 检查是否是标记图片
        name = fileObjs.name
        code = name.split('.')[0]
        # txt文件路径
        print('检查是否是标记图片')
        txt_path = file_path.replace("jpeg", "txt")
        sign = False
        sign_data = []
        if os.path.exists(txt_path):
            sign = True

        if sign:
            im = Image.open(file_path)
            width, height = im.size
            # 检查修改图片时，检查权限以及裁剪是否将标记框剪碎
            sign_data = self.getxy(txt_path, width, height)
            print(sign_data)
            status = self.checkphoto(int(left), int(top), int(width), int(height), sign_data)
            print('status', status)
            # if not status:
            #     raise Exception(u'此裁剪已破坏标记框')
        # 保存文件副本
        temp_file_path = os.path.join(temp_path, fileObjs.name)
        temp_txt_path = os.path.join(temp_path, code+'.txt')
        shutil.copy(file_path, temp_file_path)
        shutil.copy(txt_path, temp_txt_path)
        try:
            # 裁剪图片参数（xmin, ymin, xmax, ymax)
            print('裁剪图片')
            im = Image.open(file_path)
            xmin = int(left)
            ymin = int(top)
            xmax = int(left) + int(width)
            ymax = int(top) + int(height)
            crop = im.crop((xmin, ymin, xmax, ymax))
            crop.save(file_path)

            im = Image.open(file_path)
            print(im.size)
            # # 此角度是顺时针，而Image是逆时针
            angle = angle % 360
            print(angle)
            if angle == 0:
                pass
            else:
                print('旋转')
                if angle == 90:
                    out = im.transpose(Image.ROTATE_270)
                elif angle == 180:
                    out = im.transpose(Image.ROTATE_180)
                elif angle == 270:
                    print(270)
                    out = im.transpose(Image.ROTATE_90)
                else:
                    raise Exception(u'不支持此旋转')
                out.save(file_path)

            # 图片缩放
            print('图片缩放')
            im = Image.open(file_path)
            photoWidth, photoHeight = im.size
            x = int(photoWidth*scaleX)
            y = int(photoHeight*scaleY)
            out = im.resize((x, y))
            out.save(file_path)

            # 修改对应xml文件 或yolo文件
            if sign:
                setyolo(file_path, txt_path, sign_data, left, top, width, height, scaleX, scaleY, angle)
        except Exception:
            shutil.move(temp_file_path, file_path)
            shutil.move(temp_txt_path, txt_path)
            return res(code=1, msg='ERROR', action='Photo Modify')
        os.remove(temp_file_path)
        os.remove(temp_txt_path)
        return res(action='Photo Modify')
