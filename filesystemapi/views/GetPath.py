from ast import literal_eval

from django.views.generic import View
from django.utils.decorators import method_decorator

from common.httpresult import httpresult
from common.res import res

from filesystemapi.file_views.function.edit_search import EditSearch
class GetPath(View, EditSearch):

    @method_decorator(httpresult)
    def post(self, request):
        username = 'sgf'
        data = request.POST.get('data')
        # 将str转化为list
        data = literal_eval(data)
        print(data)

        files = EditSearch.getfile(self, data, username)


        return res(data=files)
