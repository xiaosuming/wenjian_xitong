from django.views import View
from django.utils.decorators import method_decorator

from common.res import res
from common.httpresult import httpresult
from filesystemapi.folder_views.function.edit_search import EditSearch

class SearchSameFolderFile(View):

    @method_decorator(httpresult)
    def get(self, request):
        username = 'sgf'
        # username = request.GET.get('username')
        folder_id = request.GET.get('folder_id')
        # type_ = request.GET.get('type_')
        # print(type(folder_id))
        data = EditSearch.searchssf(self, username, folder_id)
        return res(data=data)
