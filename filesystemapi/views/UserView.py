from django.views.generic import View
from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator

from filesystemapi.file_views.function.edit_search import EditSearch

class UserView(View, EditSearch):
    """
            @apiName UserView
            @api {GET} /filesystem/userview/
            @apiGroup user
            @apiVersion 0.0.1
            @apiDescription 获取除自身外的所有用户
            @apiParamExample {params} 请求样例：
            {
            }
            @apiSuccess (200) {String} msg 信息
            @apiSuccess (200) {String} code 0代表无错误 1代表有错误
            @apiSuccess (200) {json} data 文件信息
            @apiSuccessExample {json} 返回样例:
            {
                "code": 0,
                "msg": "SUCCESS",
                "action": "View Users",
                "data": [
                    {
                        "userId": 2,
                        "userName": "jack"
                    },
                    {
                        "userId": 3,
                        "userName": "rose"
                    }
                ]
            }
    """
    @method_decorator(httpresult)
    def get(self, request):
        # user_id = request.GET.get('user_id')
        user_id = 1
        print(user_id)
        data = EditSearch.userview(self, user_id=user_id)

        return res(action='View Users', data=data)
