from files.models.File import File
from django.contrib.auth.models import User
from django.views.generic import View
from common.httpresult import httpresult
from common.ParamMissingException import ParamMissingException
from common.res import res
from django.utils.decorators import method_decorator

class SearchFile(View):

    @method_decorator(httpresult)
    # 根据文件id查找名字
    def get(self, request):
        file_id = request.GET.get('file_id')
        file = File.objects.filter(id=file_id).first()

        data = {
            'name': file.name
        }
        return res(data=data)