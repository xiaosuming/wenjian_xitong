from django.views.generic import View
from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator
from filesystemapi.share.edit import Modify
import json


class ShareModify(View):
    """
            @apiName ShareModify
            @api {PUT} /filesystem/sharemodify/
            @apiGroup share
            @apiVersion 0.0.1
            @apiDescription 修改分享
            @apiParam {int} pId 权限id
            @apiParam {bool} right 权限，0可读，1读写
            @apiParamExample {json} 请求样例：
            {
                "pId": 1,
                "right": 1
,                }
            @apiSuccess (200) {String} msg 信息
            @apiSuccess (200) {String} code 0代表无错误 1代表有错误
            @apiSuccessExample {json} 返回样例:
            {
                "code": 0,
                "msg": "SUCCESS",
                "action": "Modify Share",
                "data": {
                    "pId": 1,
                    "user": "sgf",
                    "userId": 1,
                    "right": 1,
                    "Id": 16,
                    "Name": "s1",
                    "isFolder": true
                }
            }
    """
    @method_decorator(httpresult)
    def put(self, request):
        data = json.loads(request.body)
        pId = data.get('pId')
        right = data.get('right')

        # test
        username = 'sgf'

        data = Modify.modify(self, pId, right, username)
        return res(action='Modify Share', data=data)
