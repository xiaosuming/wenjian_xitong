from django.views.generic import View
from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator
from filesystemapi.share.add import Add


class ShareAdd(View):
    """
        @apiName ShareAdd
        @api {POST} /filesystem/shareadd/
        @apiGroup share
        @apiVersion 0.0.1
        @apiDescription 增加分享文件夹及文件
        @apiParam {string} urlList 资源url列表；可以是文件，也可以是文件夹，若是文件夹只能是一个url
        @apiParam {string} userId分享给某些用户的id的list
        @apiParam {bool} right 用户获得权限；0，可读；1，读写
        @apiParamExample {form-data} 请求样例：
        {
            "url":[xxx/xxx/xxx,xxx/xxx/xxx,]
            "usersId": [2,3],
            "right"，True
,                }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Add Share",
            "data": {
                "userNames": [
                    "jack",
                    "rose"
                ],
                "userIds": [
                    2,
                    3
                ],
                "right": "1"
            }
        }
    """
    @method_decorator(httpresult)
    def post(self, request):
        urls = request.POST.get('urlList')
        # folderId = request.POST.get('folderId')
        usersId = request.POST.get('usersId')
        right = request.POST.get('right')
        print(urls)
        # test
        username = 'sgf'

        perm_info = Add.shareadd(self, urls, usersId, right, username)
        return res(action='Add Share', data=perm_info)
