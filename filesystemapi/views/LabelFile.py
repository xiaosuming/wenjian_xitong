from django.views import View
from django.utils.decorators import method_decorator

from common.res import res
from common.httpresult import httpresult

from filesystemapi.file_views.function.edit_create import EditCreate

class LabelFile(View):

    @method_decorator(httpresult)
    def post(self, request):
        username = 'sgf'
        # username = request.POST.get('username')
        folder_id = request.POST.get('folder_id')
        file_ = request.FILES.get('file_')

        data = EditCreate.hdfsadd(self, folder_id, file_, username)

        return res(action='label file', data=data)




