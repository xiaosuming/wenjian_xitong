from files.models.Folder import Folder
from django.contrib.auth.models import User
from files.models.File import File
from files.models.Permission import Permission
from django.views.generic import View
from common.ParamMissingException import ParamMissingException
from common.httpresult import httpresult
from common.getpath import getpath
from common.res import res
from django.utils.decorators import method_decorator
import os


class Test1(View):
    """
        @apiName ShareAdd
        @api {POST} /filesystem/shareadd/
        @apiGroup share
        @apiVersion 0.0.1
        @apiDescription 增加分享文件夹及文件
        @apiParam {string} urlList 资源url列表；可以是文件，也可以是文件夹，若是文件夹只能是一个url
        @apiParam {string} userId分享给某些用户的id的list
        @apiParam {bool} right 用户获得权限；0，可读；1，读写
        @apiParamExample {form-data} 请求样例：
        {
            "url":[xxx/xxx/xxx,xxx/xxx/xxx,]
            "usersId": [2,3],
            "right"，True
,                }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Add Share",
            "data": {
                "userNames": [
                    "jack",
                    "rose"
                ],
                "userIds": [
                    2,
                    3
                ],
                "right": "1"
            }
        }
    """
    @method_decorator(httpresult)
    def post(self, request):
        urls = request.POST.get('urlList')
        folderId = request.POST.get('folderId')
        usersId = request.POST.get('usersId')
        right = request.POST.get('right')

        # test
        username = 'jack'
        user = User.objects.filter(username=username).first()

        # url列表
        urlList = []
        urlTemp = eval(urls)
        if type(urlTemp) is str:
            urlList.append(urlTemp)
        else:
            urlList = list(urlTemp)

        # 分享受用者list
        usersIdList = []
        tempList = eval(usersId)

        if type(tempList) is int:
            usersIdList.append(tempList)
        else:
            usersIdList = list(tempList)

        if len(urlList) == 0:
            raise ParamMissingException(data=u'缺少url')
        if len(usersIdList) == 0:
            raise ParamMissingException(data=u'缺少使用者名')

        userObjList = []
        userNameList = []
        for userId in usersIdList:
            userObj = User.objects.filter(id=userId).first()
            if userObj is None:
                raise Exception(u'此用户不存在')
            userNameList.append(userObj.username)
            userObjList.append(userObj)
        url = urlList[0]
        # 用户根目录
        user_path = getpath('media', str(user.username))
        path = os.path.join(user_path, url)
        print(path)
        if not os.path.exists(path):
            raise Exception(u'资源不存在')
        perm_info = {
            'userNames': userNameList,
            'userIds': usersIdList,
            'right': right,
        }
        folderObj = Folder.objects.filter(url=url).first()
        # 是文件夹
        if len(urlList) == 1 and folderObj:
            print('folder')
            for userObj in userObjList:
                permissionObj = Permission.objects.filter(user=userObj, folder=folderObj).first()
                # 若存在就修改， 否则新建
                if permissionObj:
                    permissionObj.right = right
                    permissionObj.save()
                else:
                    newPermissionObj = Permission.objects.create(user=userObj, folder=folderObj, right=right)
            perm_info['folderId'] = folderObj.id
        else:
            print('file')
            fileIdList = []
            for url in urlList:
                fileObj = File.objects.filter(url=url).first()
                if fileObj is None:
                    raise Exception(u'文件不存在')
                fileIdList.append(fileObj.id)
                for userObj in userObjList:
                    permissionObj = Permission.objects.filter(user=userObj, file=fileObj).first()
                    if permissionObj:
                        permissionObj.right = right
                        permissionObj.save()
                    else:
                        newPermissionObj = Permission.objects.create(user=userObj, file=fileObj, right=right)
            perm_info['fileId'] = fileIdList

        return res(action='Add Share', data=perm_info)
