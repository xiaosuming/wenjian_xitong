from django.views.generic import View
from django.contrib.auth.models import User
from common.httpresult import httpresult
from django.utils.decorators import method_decorator
from common.res import res
from common.getxml import getxml
from common.getyolo import getyolo


class SignView(View):
    """
            @apiName SignView
            @api {GET} /filesystem/signview/
            @apiGroup sign
            @apiVersion 0.0.1
            @apiDescription 预览标记图片
            @apiParam {String} code 文件名(不带后缀）
            @apiParam {String} type 解析文件类型（‘xml','yolo')
            @apiParamExample {params} 请求样例：
            {
                "code":"cat",
                "type": "xml"
            }
            @apiSuccess (200) {String} msg 信息
            @apiSuccess (200) {String} code 0代表无错误 1代表有错误
            @apiSuccessExample {json} 返回样例:
            {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Sign View",
            "data": {
                "url": "sign/sgf/cat/cat.jpeg",
                "data": [
                    {
                        "name": "cat",
                        "cubeStyle": {
                            "height": "215px",
                            "width": "121px",
                            "left": "181px",
                            "top": "45px"
                        }
                    },
                    {
                        "name": "cat",
                        "cubeStyle": {
                            "height": "215px",
                            "width": "121px",
                            "left": "181px",
                            "top": "45px"
                        }
                    }
                ]
            }
            }
    """
    @method_decorator(httpresult)
    def get(self, request):
        code = request.GET.get('code')
        type = request.GET.get('type')
        # test
        username = 'sgf'
        user = User.objects.filter(username=username).first()

        download_url = ''
        info = ''
        if type == 'xml':
            info, download_url = getxml(code, user.username)
        elif type == 'yolo':
            info, download_url = getyolo(code, user.username)

        data = {
            'url': download_url,
            'data': info
        }
        return res(action='Sign View', data=data)
