from files.models.Folder import Folder
from files.models.File import File
from files.models.Permission import Permission
from django.contrib.auth.models import User
from django.views.generic import View
from common.httpresult import httpresult
from common.startend import startEnd
from common.checkpermission import foldercheckpermission
from common.ParamMissingException import ParamMissingException
from common.getfilesize import getfilesize
from common.res import res
from django.utils.decorators import method_decorator
from django.db.models import Q


class Test(View):
    """
        @apiName SearchCommon
        @api {GET} /filesystem/searchcommon/
        @apiGroup Search
        @apiVersion 0.0.1
        @apiDescription 搜索文件夹及文件（名字）
        @apiParam {int} folderId 文件夹id，要搜索的文件
        @apiParam {string} keyWord 搜索关键字
        @apiParam {int} pageSize 页面大小，一页显示多少条数据
        @apiParam {int} page 页码,分页显示排序结果
        @apiParamExample {params} 请求样例：
        {
            "folderId": 1,
            "keyWord": "hello",
            "pageSize": 20,
            "page": 1,
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Search",
            "data": {
            "url": "/sgf/其他/其他",
            "depth": 3，
            "isFinished": False,
            'file":
            [
                {
                "fileName": "q1",
                "fileId": 29,
                "dateCreated": "2019-08-02",
                "dateModified": "2019-08-02",
                "fileSize": "0b",
                "filePath": "/sgf/其他/其他",
                "fileType": null，
                },
                ...
            ],
        }
    """
    def foldersearch(self, folderObj, keyword):
        fileObjs = File.objects.filter(Q(url__icontains=folderObj.name),
                                       Q(name__icontains=keyword))
        folderObjs = Folder.objects.filter(Q(url__icontains=folderObj.name),
                                           Q(name__icontains=keyword))
        return fileObjs, folderObjs
    @method_decorator(httpresult)
    def get(self, request):
        folderId = request.GET.get('folderId')
        keyword = request.GET.get('keyWord')
        page = request.GET.get('page')
        pagesize = request.GET.get('pageSize')
        keyword = keyword.lower()
        pagesize = int(pagesize)
        page = int(page)

        if keyword is None:
            raise ParamMissingException(data=u'缺少关键字')
        if folderId is None:
            raise ParamMissingException(data=u'缺少父目录id')

        folderObj = Folder.objects.filter(id=folderId).first()
        if folderObj is None:
            raise Exception(u'父目录不存在')

        # test
        username = 'sgf'
        user = User.objects.filter(username=username).first()

        # 检查当前用户对此目录是否有权限
        folderCreater = folderObj.creater
        if user != folderCreater:
            p = foldercheckpermission(folderObj, user)
            if p is -1:
                return res(code=1, msg="ERROR", action="Search Common", data=u'此用户没有权限')
        # fileObjs = []
        # folderObjs = []
        if folderObj.depth == 1 and folderObj.name == u'与我分享的':
            fileObjs = File.objects.filter(name__icontains=keyword, permissionFile__user=user)
            if keyword == '':
                folderObjs = []
            else:
                # #与我分享的，下一级搜索结果
                # 每个文件夹的搜索
                folderObjs = Folder.objects.filter(name__icontains=keyword, permissionFolder__user=user)
                for folderObj in folderObjs:
                    fileObjs1, folderObjs1 = self.foldersearch(folderObj, keyword)
                    fileObjs.extend(fileObjs1)
                    folderObjs.extend(folderObjs1)

        else:
            if keyword == '':
                fileObjs = File.objects.filter(Q(pfolder=folderObj),
                                               Q(name__icontains=keyword))
                folderObjs = []
            else:
                fileObjs = File.objects.filter(Q(url__icontains=folderObj.name),
                                               Q(name__icontains=keyword))
                folderObjs = Folder.objects.filter(Q(url__icontains=folderObj.name),
                                           Q(name__icontains=keyword))
        # 删除重复元素
        fileObjs = list(set(fileObjs))
        folderObjs = list(set(folderObjs))

        data = {}
        objs = []
        for file in fileObjs:
            size = getfilesize(file.size)
            fileInfo = {
                'fileName': file.name,
                'fileId': file.id,
                'dateCreated': file.createtime,
                'dateModified': file.modifytime,
                'fileSize': size,
                'filePath': file.url,
                'fileType': file.filetype,
                'isFolder': False
            }
            objs.append(fileInfo)
        for folder in folderObjs:
            folderInfo = {
                'fileName': folder.name,
                'fileId': folder.id,
                'dateCreated': folder.createtime,
                'dateModified': None,
                'fileSize': None,
                'filePath': folder.url,
                'fileType': None,
                'isFolder': True
            }
            objs.append(folderInfo)
        count = len(objs)
        start, end = startEnd(page, count, pagesize)
        # 分页返回
        data['objs'] = objs[start:end]
        data['isFinish'] = False
        if end == count:
            data['isFinish'] = True
        return res(action='Search Common', data=data)
