
from django.views.generic import View

from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator
from filesystemapi.share.delete import Delete

class ShareDel(View):
    """
        @apiName ShareDel
        @api {DELETE} /filesystem/sharedel/
        @apiGroup share
        @apiVersion 0.0.1
        @apiDescription 删除分享文件夹或文件
        @apiParam {int} pId 权限id列表
        @apiParamExample {params} 请求样例：
        {
            "pId": [1,2,3]
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Delete Share",
            "data": [],
        }
    """
    @method_decorator(httpresult)
    def delete(self, request):
        pId = request.GET.get('pId')

        pId = eval(pId)
        # test
        username = 'sgf'

        Delete.sharedel(self, pId, username)
        return res(action='Delete Share')
