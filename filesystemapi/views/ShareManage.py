
from django.views.generic import View
from common.httpresult import httpresult
from common.res import res

from django.utils.decorators import method_decorator
from filesystemapi.share.manage import Manage

class ShareManage(View, Manage):
    """
        @apiName ShareManage
        @api {GET} /filesystem/shareManage/
        @apiGroup share
        @apiVersion 0.0.1
        @apiDescription 获取分享文件夹
        @apiParamExample {params} 请求样例：
        {
,        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Share Manage",
            "data": [
                {
                    "user": "rose",
                    "folderId": 4,
                    "folderName": "音频",
                    "right": true
                },
                {
                    "user": "rose",
                    "folderId": 5,
                    "folderName": "其他",
                    "right": false
                },
                {
                    "user": "jack",
                    "folderId": 7,
                    "folderName": "测试",
                    "right": false
                },
                {
                    "user": "jack",
                    "folderId": 14,
                    "folderName": "hello",
                    "right": true
                }
            ]
        }
    """


    @method_decorator(httpresult)
    def get(self, request):
        page = request.GET.get('page')
        pagesize = request.GET.get('pageSize')
        pagesize = int(pagesize)
        page = int(page)
        # test
        username = 'sgf'
        data = Manage.sharemanage(self, pagesize, page, username)
        return res(action='Share Manage', data=data)
