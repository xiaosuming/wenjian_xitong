from files.models.File import File
from files.models.Folder import Folder
from django.contrib.auth.models import User
from django.views.generic import View
from common.httpresult import httpresult
from common.ParamMissingException import ParamMissingException
from common.res import res
from django.utils.decorators import method_decorator

class SearchAllFile(View):

    def child(self, foldid, folders_list):
        print('开始递归', folders_list, foldid, '---')
        folder = Folder.objects.filter(parent_id=foldid)
        # print(folder)
        if folder is None:
            return folders_list

        # 找最下层文件夹
        for item in folder:
            print('minigzi:', item)
            file_obj = File.objects.filter(pfolder_id=item)
            print(10*'24-', file_obj)
            for file in file_obj:
                print('名字:', item)

                if file is None:
                    continue

                data = {
                    'id': file.id,
                    'name': file.name,
                    # 'url': file.url
                }
                print(10*'31-', data)
                folders_list.append(data)
                print(folders_list)

            folder = Folder.objects.filter(parent_id=item).first()
            print(10*'34-', folder)
            if folder is None:
                continue
            self.child(foldid=item, folders_list=folders_list)
        return folders_list

    @method_decorator(httpresult)
    def get(self, request):

        folder_id = request.GET.get('folder_id')
        file_obj = File.objects.filter(pfolder_id=folder_id)


        folders_list = []
        # 子文件夹文件信息
        for item in file_obj:
            data = {
                'id': item.id,
                'name': item.name,
            }
            folders_list.append(data)

        data = self.child(foldid=folder_id, folders_list=folders_list)
        # print(data)

        return res(data=data)