
from django.views.generic import View
from common.httpresult import httpresult

from common.res import res
from django.utils.decorators import method_decorator

from filesystemapi.folder_views.function.edit_search import EditSearch

class SearchCommon(View, EditSearch):
    """
        @apiName SearchCommon
        @api {GET} /filesystem/searchcommon/
        @apiGroup Search
        @apiVersion 0.0.1
        @apiDescription 搜索文件夹及文件（名字）
        @apiParam {int} folderId 文件夹id，要搜索的文件
        @apiParam {string} keyWord 搜索关键字
        @apiParam {int} pageSize 页面大小，一页显示多少条数据
        @apiParam {int} page 页码,分页显示排序结果
        @apiParamExample {params} 请求样例：
        {
            "folderId": 1,
            "keyWord": "hello",
            "pageSize": 20,
            "page": 1,
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Search",
            "data": {
            "url": "/sgf/其他/其他",
            "depth": 3，
            "isFinished": False,
            'file":
            [
                {
                "fileName": "q1",
                "fileId": 29,
                "dateCreated": "2019-08-02",
                "dateModified": "2019-08-02",
                "fileSize": "0b",
                "filePath": "/sgf/其他/其他",
                "fileType": null，
                },
                ...
            ],
        }
    """

    @method_decorator(httpresult)
    def get(self, request):
        folderId = request.GET.get('folderId')
        keyword = request.GET.get('keyWord')
        page = request.GET.get('page')
        pagesize = request.GET.get('pageSize')
        keyword = keyword.lower()
        pagesize = int(pagesize)
        page = int(page)

        data = EditSearch.searchcommon(self, folderId, keyword, pagesize, page)
        print('ss', data)
        return res(action='Search Common', data=data)
