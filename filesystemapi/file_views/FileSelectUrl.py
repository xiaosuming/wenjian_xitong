from django.views.generic import View
from django.utils.decorators import method_decorator

from common.httpresult import httpresult
from common.res import res

from filesystemapi.file_views.function.edit_search import EditSearch

class FileSelectUrl(View):
    # 获取文件地址
    @method_decorator(httpresult)
    def post(self, request):
        username = 'sgf'
        file_id = request.POST.get('data')
        data = EditSearch.filesurl(self, file_id, username)
        return res(data=data)