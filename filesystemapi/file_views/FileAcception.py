from django.contrib.auth.models import User
from django.views.generic import View
from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator

from filesystem.settings import CLIENT

class FileAcception(View):
    """
        @apiName FileAcception
        @api {POST} /filesystem/fileload/
        @apiGroup file
        @apiVersion 0.0.1
        @apiDescription 上传分片
        @apiParam {String} identifier 分片标识符
        @apiParam {int} chunkSize 分片大小，单位字节
        @apiParam {int} totalSize  所有分片大小
        @apiParam {int} totalChunks 分片数目
        @apiParam {String} file 二进制格式的分片内容
        @apiParamExample {form-data} 请求样例：
        {

        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data upload success!
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "UpLoad Chunks",
            "data": [],
        }
    """
    @method_decorator(httpresult)
    def post(self, request):
        global file_path
        print('接受分片开始')
        identifier = request.POST.get('resumableIdentifier')
        chunknumber = request.POST.get('resumableChunkNumber')
        totalchunks = request.POST.get('resumableTotalChunks')
        file = request.FILES['file'].file.read()
        # test
        username = 'sgf'
        user = User.objects.filter(username=username).first()

        # 组装uuid
        uuid = identifier
        # 分片名：分片序号
        chunkname = chunknumber
        # print('uuid', uuid)
        # print('chunkname', chunkname)

        try:
            folder_path = 'Acceptation/' + identifier
            file_path =   folder_path + '/' + chunknumber
            print(folder_path)
            if CLIENT.status(hdfs_path=folder_path, strict=False) is None:
                CLIENT.makedirs(hdfs_path='/Acceptation/%s' % identifier)
            CLIENT.write(hdfs_path=file_path,data=file)
        except Exception:
            CLIENT.delete(hdfs_path=file_path)
            raise Exception(u'上传分片失败')
            # return res(code=1, msg='ERROR', action='UpLoad Chunks')
        data = {
            'isFinish': False
        }
        if chunknumber == totalchunks:
            print('分片全部成功')
            data['isFinish'] = True
        print('分片成功')
        return res(action='UpLoad Chunks', data=data)

