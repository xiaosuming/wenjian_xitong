from django.views.generic import View
from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator
from filesystemapi.check.file import FileCheck

class FileCheckPaste(View):
    """
        @apiName FileCheckPaste
        @api {POST} /filesystem/filecheckpaste/
        @apiGroup check
        @apiVersion 0.0.1
        @apiDescription 移动及复制文件时，检查文件是否重名和权限
        @apiParam {list} filesId 文件id列表
        @apiParam {id} folderId 目的文件夹id
        @apiParamExample {form-data} 请求样例：
        {
            "filesId": ['85','86']，
            "folderId": 1
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Check Paste File",
            "data": [
                {
                    "id": 72,
                    "duplicate": true,
                    "right": true
                },
                {
                    "id": 73,
                    "duplicate": true,
                    "right": true
                },
                {
                    "id": 74,
                    "duplicate": false,
                    "right": true
                },
                {
                    "id": 75,
                    "duplicate": false,
                    "right": true
                },
                {
                    "id": 76,
                    "duplicate": false,
                    "right": true
                }
            ]
        }
    """
    @method_decorator(httpresult)
    def post(self, request):
        filesId = request.POST.get('filesId')
        dstFolderId = request.POST.get('folderId')

        # test
        username = 'sgf'
        data = FileCheck.checkpaste(self, filesId, dstFolderId, username)
        return res(action='Check Paste File', data=data)
