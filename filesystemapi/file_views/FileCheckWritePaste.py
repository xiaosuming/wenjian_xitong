from files.models.Folder import Folder
from files.models.File import File
from django.views.generic import View
from django.contrib.auth.models import User
from common.ParamMissingException import ParamMissingException
from common.checkpermission import onefilecheckpermission
from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator


class FileCheckWritePaste(View):
    """
        @apiName FileCheckWritePaste
        @api {POST} /filesystem/filecheckwritepaste/
        @apiGroup check
        @apiVersion 0.0.1
        @apiDescription 移动及复制文件时，检查文件是否重名和权限
        @apiParam {list} filesId 文件id列表
        @apiParam {id} folderId 目的文件夹id
        @apiParamExample {form-data} 请求样例：
        {
            "filesId": ['85','86']，
            "folderId": 1
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Check Paste File",
            "data": {
                "id": [
                    "85",
                    "86"
                ],
                "count": 2
            }
        }
    """
    @method_decorator(httpresult)
    def post(self, request):
        filesId = request.POST.get('filesId')
        dstFolderId = request.POST.get('folderId')

        # test
        username = 'sgf'
        user = User.objects.filter(username=username).first()

        if filesId is None:
            raise ParamMissingException(data=u'缺少文件位置信息')

        if dstFolderId is None:
            raise ParamMissingException(data=u'缺少文件位置信息')

        filesList = []
        # 转换成列表
        filestemp = eval(filesId)

        # 是一个id
        if type(filestemp) is int:
            filesList.append(filestemp)
        else:
            filesList = list(filestemp)

        folderObj = Folder.objects.filter(id=dstFolderId).first()
        if folderObj is None:
            raise Exception(u'目录不存在')

        idList = []
        for objId in filesList:
            fileObj = File.objects.filter(id=objId).first()
            if fileObj is None:
                raise Exception(u'文件不存在')
            # 检查权限
            # 检查当前用户对此目录是否有写权限
            fileCreater = fileObj.creater
            if user != fileCreater:
                p = onefilecheckpermission(fileObj, user)
                if p is not True:
                    continue
            # 检查重名
            name = fileObj.name
            folderObjs = Folder.objects.filter(name=name, parent=folderObj)
            fileObjs = File.objects.filter(name=name, pfolder=folderObj)

            if folderObjs.count() == 0 and fileObjs.count() == 0:
                idList.append(objId)
        data = {
            'id': idList,
            'count': len(idList)
        }
        return res(action='Check Paste File', data=data)
