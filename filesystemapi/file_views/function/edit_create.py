import os

from django.contrib.auth.models import User

from common.res import res
from common.getpath import getpath
from common.ParamMissingException import ParamMissingException

from files.models.Folder import Folder
from files.models.File import File
from common.filemd5 import md5

from filesystem.settings import CLIENT

class EditCreate(object):

    # 新建文件
    def fileadd(self, pfolderId, filename, username):

        user = User.objects.filter(username=username).first()
        pfolder = Folder.objects.filter(id=pfolderId).first()

        if pfolder is None:
            raise Exception(u'父目录不存在')

        url = pfolder.url
        print('url:', url)
        depth = pfolder.depth + 1

        if filename is None:
            filename = u'新建文件'

        fileObjs = File.objects.filter(name=filename, pfolder=pfolder, creater=user)
        if fileObjs.count() != 0:
            fileObjs.delete()

        root_path = getpath('media', str(pfolder.creater.username))
        # 如有重名,删除
        path = os.path.join(root_path, url, filename)
        # path = user_path + url + URL_SEPARTOR + filename
        if os.path.exists(path):
            os.remove(path)
        print('path:', path)

        try:
            # 在数据库中添加记录
            file = File.objects.create(name=filename, pfolder=pfolder, depth=depth, creater=pfolder.creater,
                                       url=pfolder.url + '/' + filename)
            # 生成文件
            f = open(path, 'w')
            f.close()
        except Exception:
            if os.path.exists(path):
                os.remove(path)
            raise Exception(u'新建文件失败')
            # return res(code=1, msg='ERROR', action='Add File')

        data = {
            'fileId': file.id,
            'fileName': file.name,
            'fileUrl': file.url,
            'depth': file.depth,
        }
        return data

    # 组合分片，生成新的文件
    def fileload(self, folderId, identifier, filename, totalchunk, type):
        # test
        username = 'sgf'
        user = User.objects.filter(username=username).first()
        print('hashid', identifier)
        print('folderId:', folderId)
        # 不能缺少
        if folderId is None:
            raise ParamMissingException(data=u'缺少父目录id')
        if identifier is None:
            raise ParamMissingException(data=u'缺少identifier')

        # 可选
        if filename is None:
            filename = u'新建文件'

        pfolder = Folder.objects.filter(id=folderId).first()
        if pfolder is None:
            raise Exception(u'父目录不存在')
        fileObjs = File.objects.filter(name=filename, pfolder=pfolder)
        if fileObjs.count() != 0:
            fileObjs.delete()

        root_path = getpath('media', str(pfolder.creater.username))
        temp_user_path = getpath('temp', str(user.username))
        # 得到url
        url = pfolder.url
        file_path = os.path.join(root_path, url)
        file_url = os.path.join(url, filename)

        temp_path = os.path.join(temp_user_path, identifier)
        print('temp_path_2', temp_path)
        # 检查分片是否完整
        print('检查分片')
        count = int(totalchunk)
        files = os.listdir(temp_path)
        lens = len(files)
        print('lens:', lens)
        print('count:', count)
        if count != lens:
            return res(action='Upload File', msg='ERROR', code=1, data=u'分片缺失,传输失败')
        # # print('结束检查分片： ', isall)
        # if isall is False:
        #     return res(action='Upload File', msg='ERROR', code=1, data=u'分片缺失,传输失败')
        totalsize = self.packagefile(filename=filename, temp_path=temp_path, file_path=file_path)

        # 得到depth
        depth = pfolder.depth + 1

        # 数据库中新建file
        fileObj = File.objects.create(name=filename, pfolder=pfolder, url=file_url,
                                      depth=depth, size=totalsize, creater=pfolder.creater, filetype=type)

        data = {
            'fileName': fileObj.name,
            'fileId': fileObj.id,
            'dateCreated': fileObj.createtime,
            'dateModified': fileObj.modifytime,
            'fileSize': totalsize,
            'filePath': file_url,
            'fileType': fileObj.filetype

        }
        print('上传完成')
        return data

    # 上传分片
    def packagefile(self, filename, temp_path, file_path):
        # 将当前目录下的所有文件合在list中
        files = os.listdir(temp_path)
        print(files)
        files.sort()
        # 打开最终文件
        f_path = os.path.join(file_path, filename)
        print(f_path)
        # flag=1 重名覆盖（删除）
        if os.path.exists(f_path):
            os.remove(f_path)
        fd = open(f_path, 'wb+')
        # 处理分片
        size = 0
        for chunk in files:
            try:
                chunk_path = os.path.join(temp_path, chunk)
                size += os.path.getsize(chunk_path)
                f = open(chunk_path, 'rb')
                data = f.read()
                fd.write(data)
                f.close()
            except OSError:
                raise Exception('组合分片失败')
        fd.close()

        # 删除临时文件
        for file in files:
            os.remove(os.path.join(temp_path, file))
        # 删除临时文件夹
        os.rmdir(temp_path)
        return size



    # hdfs上传文件
    def hdfsadd(self, folder_id, file_, username):
        # 获取文件名和类型
        name = file_.name
        file_type = name.split('.')[1]
        data = []
        user = User.objects.filter(username=username).first()
        folder = Folder.objects.filter(id=folder_id).first()
        files = File.objects.filter(name=name,pfolder_id=folder).count()
        if folder is None:
            raise Exception('该文件夹不存在')

        # 对文件进行md5
        MD5 = md5(file_)
        user_id = int(user.id)
        hdfs_names = CLIENT.list('/')

        # hdfs中有内容相同的文件，只添加数据库中的信息
        if MD5 in hdfs_names:
            # print('hdfs_names', hdfs_names)
            # print('MD5', MD5)
            if files is 1:
                raise Exception(u'%s文件已存在' % name)
            else:
                datas = CLIENT.status(MD5)
                size = datas['length']
                file = File.objects.create(name=file_.name, url=folder.url + '/' + file_.name,
                                           size=size, depth=folder.depth + 1,
                                           creater_id=user_id, pfolder_id=folder.id, filetype=file_type,
                                           hdfsName=MD5)
                message = {
                    'file_id': file.id,
                    'file_name': file.name,
                    'file_url': file.url,
                }
                data.append(message)
        # hdfs中没有相同的文件，添加到hdfs中并添加到数据库
        else:
            try:
                CLIENT.write(hdfs_path=MD5,data=file_)
                print('文件上传成功')
                datas = CLIENT.status(MD5)
                size = datas['length']
                file = File.objects.create(name=file_.name, url=folder.url + '/' + file_.name,
                                           size=size, depth=folder.depth + 1,
                                           creater_id=user_id, pfolder_id=folder.id, filetype=file_type,
                                           hdfsName=MD5)
                message = {
                    'file_id' : file.id,
                    'file_name' : file.name,
                    'file_url' : file.url,
                }
                data.append(message)
            except Exception as e:
                print(e)
                return Exception(u'上传文件失败d')
        return data