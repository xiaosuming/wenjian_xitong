import os

from django.contrib.auth.models import User
from django.http import StreamingHttpResponse
from common.checkpermission import onefilecheckpermission
from common.getpath import getpath
from common.res import res

from files.models.File import File
from filesystem.settings import CLIENT
import io

import logging
import traceback
def filedownload(filesId, username):
    user = User.objects.filter(username=username).first()

    # 如果为多文件则转换成列表
    if type(eval(filesId)) is tuple:
        files = list(eval(filesId))
        print(files)
        return filesdownload(files, username)

    fileObj = File.objects.filter(id=filesId).first()
    # print(fileObj)
    if fileObj is None:
        return res(code=1, msg="ERROR", action="Down Load File", data=u'没有该文件')
    if user != fileObj.creater:
        right = onefilecheckpermission(fileObj, user)
        # 没有权限，暂时先报错，可以改成跳过
        if right is not True:
            return res(code=1, msg="ERROR", action="Down Load File", data=u'此用户没有权限')

    hdfs_name = fileObj.hdfsName
    the_file_name = fileObj.name
    size = fileObj.size
    # 从hdfs下载文件
    try:
        with CLIENT.read(hdfs_name) as reader:
            # 这里创建返回
            response = StreamingHttpResponse(io.BytesIO(reader.data))
            # 注意格式
            response['Content-Type'] = 'image/jpg'
            # if preview is True:
            #     print('预览模式')
            #     # 注意filename 这个是预览后的名字
            #     response['Content-Disposition'] = 'inline;filename=%s' % the_file_name
            # 注意filename 这个是下载后的名字
            response['Content-Disposition'] = 'attachment;filename=%s' % the_file_name
            # # 文件长度
            response['content-length'] = size
            return response

    except Exception:
        traceback.print_exc()
        logger = logging.getLogger('django')
        logger.error(traceback.format_exc())
        return res(code=1, msg="ERROR", action="Down Load File", data=u'下载失败')

def filesdownload(files, username):
    print('进入多文件下载模式')
    path = getpath('media', 'username', 'zip')
    if os.path.isdir(path) is False:
        os.mkdir(path)
    for item in files:
        file = File.objects.filter(id=item).first()
        file_path = os.path.join(path, file.name)

