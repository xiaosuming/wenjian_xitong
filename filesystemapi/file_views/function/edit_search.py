from django.contrib.auth.models import User

from common.getpath import getpath
from common.startend import startEnd
from common.getfilesize import getfilesize
from common.ParamMissingException import ParamMissingException

from files.models.Permission import Permission
from files.models.Folder import Folder
from files.models.File import File
from ast import literal_eval
# from filesystem.settings import IP
import os
class EditSearch(object):


    # 除此用户的其他用户信息
    def userview(self, user_id):
        user = User.objects.filter(id=user_id).first()
        userList = User.objects.exclude(id=user.id)
        data = []
        for user in userList:
            user_info = {
                'userId': user.id,
                'userName': user.username,
            }
            data.append(user_info)
        return data


    # 文件排序
    def filesort(self, folderId, sort_type, sort_order, page, pagesize):
        pagesize = int(pagesize)

        # test
        username = 'sgf'
        user = User.objects.filter(username=username).first()

        print('filesort')
        if folderId is None:
            raise ParamMissingException(data=u'缺少目录id')
        if sort_type is None:
            raise ParamMissingException(data=u'缺少排序方法')

        folderObj = Folder.objects.filter(id=folderId).first()
        if folderObj is None:
            raise Exception(u'文件夹不存在')

        folder_url = folderObj.url
        depth = folderObj.depth + 1
        data = {}

        print(folderObj)
        methods = {'fileName': 'name', 'fileSize': 'size', 'dateCreated': 'createtime', 'dateModified': 'modifytime'}
        print(methods)
        method = methods[sort_type]
        print('method 1', method)
        print('sort_order', sort_order)
        print('')
        reverse = False
        if method is None:
            raise Exception(u'不支持此排序')
        if sort_order == 'ascend':
            method = method
        elif sort_order == 'descend':
            reverse = True
            method = '-' + method
        else:
            raise Exception(u'不支持此排序方向')

        print('sortMethod:', method)
        fd = []
        if folderObj.depth == 1 and folderObj.name == u'与我分享的':
            permissionFiles = Permission.objects.filter(user=user, folder=None)
            count = permissionFiles.count()
            print('count', count)
            for p in permissionFiles:
                file = p.file
                size = getfilesize(file.size)
                permissim_info = {
                    'fileName': file.name,
                    'fileId': file.id,
                    'dateCreated': file.createtime,
                    'dateModified': file.modifytime,
                    'fileSize': size,
                    # 'filePath': url,
                    'fileType': file.filetype,
                    'isShare': True
                }
                fd.append(permissim_info)
                fd = sorted(fd, key=lambda keys: keys[sort_type], reverse=reverse)
        else:
            fileObjs = File.objects.filter(pfolder=folderObj).order_by(method)
            count = fileObjs.count()
            for file in fileObjs:
                size = getfilesize(file.size)
                url = file.url
                fileInfo = {
                    'fileName': file.name,
                    'fileId': file.id,
                    'dateCreated': file.createtime,
                    'dateModified': file.modifytime,
                    'fileSize': size,
                    'filePath': url,
                    'fileType': file.filetype,
                    'isShare': False
                }
                fd.append(fileInfo)
        # 分片列表
        start, end = startEnd(page, count, pagesize)
        print('count', count)
        print(start, end)
        # 分页返回
        data['file'] = fd[start:end]
        data['url'] = folder_url
        data['depth'] = depth
        data['isFinished'] = False
        if end == count:
            data['isFinished'] = True
        return data


    # 获取标注文件和源文件地址
    def getfile(self, data, username):
        files = []

        for item in list(data):
            raw_id = item['raw_id']
            label_id = item['label_id']
            print('rawID:', raw_id, 'labelID:', label_id)
            ip = IP
            file_raw = File.objects.filter(id=raw_id).first()
            file_label = File.objects.filter(id=label_id).first()
            print('file_raw:', file_raw, 'file_label:', file_label)

            if file_raw is None:
                raise ParamMissingException(data=u'%sid不存在' %raw_id)

            if file_label is None:
                raise ParamMissingException(data=u'%sid不存在' %label_id)

            path = getpath('media', username, file_raw.url)
            path = ip + path
            label_path = getpath('media', username, file_label.url)
            label_path = ip + label_path
            print(path)
            data = {
                'raw': path,
                'label': label_path
            }
            files.append(data)
        return files


    # def search


    # 获取文件地址
    def filesurl(self, file_id, username):
        print(type(file_id))
        files = literal_eval(file_id)
        path_root = getpath('media', username)
        ip_ = IP
        data = []
        for item in files:

            file = File.objects.get(id=item)
            print(file)
            path = ip_ + os.path.join(path_root, file.url)

            files = {
                '%s' % file.id:path,
            }

            data.append(files)
        return data