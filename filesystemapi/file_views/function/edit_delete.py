from django.contrib.auth.models import User

from common.startend import startEnd
from common.getfilesize import getfilesize
from common.ParamMissingException import ParamMissingException

from files.models.Permission import Permission
from files.models.File import File

from filesystem.settings import CLIENT

class EditDelete(object):

    # 根据数据库判断是否删除hdfs中的文件，如果数据库中该文件只有一个删除，否则只删除数据库
    def filedelete(self, filesId, username):
        user = User.objects.filter(username=username).first()

        filesList = []
        if type(filesId) is str:
            # 转换成列表
            filestemp = eval(filesId)
            # 是一个id
            if type(filestemp) is int:
                filesList.append(filestemp)
            else:
                filesList = list(filestemp)
        for item in filesList:
            # print('item', item)
            file = File.objects.filter(id=item).first()
            # print('file', file)
            if file is None:
                raise Exception(u'id为%s的文件不存在' % item)

            file_obj = File.objects.filter(name=file.name)
            name = file.hdfsName
            # print(file_obj.count())
            try:
                print('file_obj.count()', file_obj.count())
                if file_obj.count() > 1:
                    print(u'删除数据库中的该文件')
                    file.delete()
                else:
                    print(u'删除hdfs中的该文件')
                    CLIENT.delete(hdfs_path=name)
                    file.delete()
            except Exception:
                raise Exception(u'删除%s文件失败' % name)
