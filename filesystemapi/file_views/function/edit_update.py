from django.contrib.auth.models import User
from files.models.File import File
from files.models.Folder import Folder
from files.models.Permission import Permission
from common.res import res
from common.checkpermission import foldercheckpermission
from common.ParamMissingException import ParamMissingException
from common.getpath import getpath
import os
import shutil
from filesystem.settings import CLIENT

class EditUpdate(object):
    # 文件重命名
    def filerename(self, fileId, filename, username):
        user = User.objects.filter(username=username).first()

        fileObj = File.objects.filter(id=fileId).first()
        fileObjs = File.objects.filter(pfolder_id=fileObj.pfolder_id)
        pfolder = fileObj.pfolder
        if fileObj is None:
            raise Exception(u'文件不存在')
        if pfolder is None:
            raise Exception(u'父目录不存在')


        for item in fileObjs:
            if item.name == filename:
                raise Exception(u"该文件名已存在")
        try:
            fileObj.name = filename
            fileObj.save()
        except Exception as e:
            print(e)
            raise Exception(u'文件重命名失败')

        data = {
            'fileId': fileObj.id,
            'fileName': fileObj.name,
        }

        return data

    # 文件copy
    def filecopypaste(self, srcFilesId, dstFolderId):
        print(srcFilesId, dstFolderId)
        if srcFilesId is None:
            raise Exception(u'文件id列表为空')
        srcFilesId = eval(srcFilesId)
        #test
        username = 'sgf'
        user = User.objects.filter(username=username).first()

        if srcFilesId is None:
            raise ParamMissingException(data=u'缺少源文件id')
        if dstFolderId is None:
            raise ParamMissingException(data=u'缺少目的文件夹id')

        dstFolder = Folder.objects.filter(id=dstFolderId).first()
        if dstFolder is None:
            raise Exception(u'目的文件夹不存在')

        # 检查当前用户对目的目录是否有写权限
        dstFolderCreater = dstFolder.creater
        if user != dstFolderCreater:
            p = foldercheckpermission(dstFolder, user)
            if p is not True:
                return res(code=1, msg="ERROR", action="Copy Paste File", data=u'此用户没有权限')

        # 循环依次操作文件
        data = []
        srcFilesIds = []
        if type(srcFilesId) is int:
            srcFilesIds.append(srcFilesId)

        fileObjs = File.objects.filter(pfolder_id=dstFolderId)
        file_name = []
        for item in fileObjs:
            file_name.append(item.name)

        for fileId in srcFilesIds:
            try:
                fileObj = File.objects.filter(id=fileId).first()

                if fileObj.name in file_name:
                    return res(code=1, msg="ERROR", action="Copy Paste File", data=u'文件已存在')
                if fileObj is None:
                    return res(code=1, msg="ERROR", action="Copy Paste File", data=u'文件不存在')

                # 数据库操作，复制文件对象
                fileObj.pk = None
                fileObj.pfolder = dstFolder
                fileObj.creater = dstFolder.creater
                fileObj.depth = dstFolder.depth + 1
                fileObj.url = os.path.join(dstFolder.url, str(fileObj.name))
                fileObj.save()
            except Exception:
                raise Exception(u'文件复制失败')
                # return res(code=1, msg='ERROR', action='CopyPaste File')
            fileInfo = {
                'fileId': fileObj.id,
                'fileName': fileObj.name,
                'depth': fileObj.depth,
            }
            data.append(fileInfo)
        return data

    # 文件剪贴
    def filecutpaste(self, srcFilesId, dstFolderId):
        if srcFilesId is None:
            raise Exception(u'文件id列表为空')
        srcFilesId = eval(srcFilesId)
        #test
        username = 'sgf'
        user = User.objects.filter(username=username).first()

        if srcFilesId is None:
            raise ParamMissingException(data=u'缺少源文件id')
        if dstFolderId is None:
            raise ParamMissingException(data=u'缺少目的文件夹id')

        dstFolder = Folder.objects.filter(id=dstFolderId).first()
        if dstFolder is None:
            raise Exception(u'目的文件夹不存在')

        # 检查当前用户对目的目录是否有写权限
        dstFolderCreater = dstFolder.creater
        if user != dstFolderCreater:
            p = foldercheckpermission(dstFolder, user)
            if p is not True:
                return res(code=1, msg="ERROR", action="Cut Paste File", data=u'此用户没有权限')

        user_path = getpath('media', str(user.username))
        # 保存移动的文件名和url
        tempList = []
        # 循环依次操作文件
        data = []

        srcFilesIds = []
        if type(srcFilesId) is int:
            srcFilesIds.append(srcFilesId)

        for fileId in srcFilesIds:
            try:
                fileObj = File.objects.filter(id=fileId).first()
                if fileObj is None:
                    raise Exception(u'文件不存在')
                srcCreater = fileObj.creater
                dstCreater = dstFolder.creater
                # 如果将‘与我分享的’的文件移动到用户自己目录下，将删除权限
                if srcCreater != user and dstCreater == user:
                    permissionObjs = Permission.objects.filter(file=fileObj, folder=None)
                    if permissionObjs.count != 0:
                        permissionObjs.delete()

                # 数据库操作，复制文件对象
                fileObj.pfolder = dstFolder
                fileObj.depth = dstFolder.depth + 1
                fileObj.creater = dstFolder.creater
                fileObj.url = os.path.join(dstFolder.url, str(fileObj.name))
                fileObj.save()

            except Exception:
                raise Exception(u'文件移动失败')
                # return res(code=1, msg='ERROR', action='CopyPaste File')
            fileInfo = {
                'fileId': fileObj.id,
                'fileName': fileObj.name,
                'depth': fileObj.depth,
            }
            data.append(fileInfo)

        return data

