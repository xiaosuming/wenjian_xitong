from django.views.generic import View
from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator
from filesystemapi.file_views.function.edit_delete import EditDelete

class FileDel(View):
    """
        @apiName FileDel
        @api {DELETE} /filesystem/filedel/
        @apiGroup file
        @apiVersion 0.0.1
        @apiDescription 批量删除文件,可删除不同目录下的文件
        @apiParam {list} filesId 文件id列表
        @apiParamExample {params} 请求样例：
        {
            "filesId":[1,2]
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data 文件信息
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Delete File",
            "data": [],
        }
    """

    @method_decorator(httpresult)
    def delete(self, request):
        # 文件名需后缀
        filesId = request.GET.get('filesId')
        # test
        username = 'sgf'
        EditDelete.filedelete(self, filesId, username)
        return res(action='Delete File')
