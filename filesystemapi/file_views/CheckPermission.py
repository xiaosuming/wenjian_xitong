import json
from django.views import View
from django.utils.decorators import method_decorator
from django.contrib.auth.models import User

from common.getpath import getpath
from common.res import res
from common.httpresult import httpresult

from files.models.Permission import Permission
from files.models.Folder import Folder

class CheckPermission(View):

    @method_decorator(httpresult)
    def put(self, request):
        data = json.loads(request.body)
        folders = data.get('folders')
        username = data.get('username')

        data = []
        for item in folders:
            # 把绝对路径转化为到用户的笑对路径
            path = getpath('media').split('/')
            item = str(item).split('/')
            i = len(item)
            p = len(path)
            s = slice(p, i)
            item = item[s]
            url = '/'.join(item[1:])
            folder = Folder.objects.filter(url=url).first()

            # 判断该文件夹是否是该用户创建
            if item[0] == username:
                folder_data = {
                    'folder': folder.id,
                    'folder_name': folder.name,
                    'permission': 1,
                }
                data.append(folder_data)
            else:
                permission = Permission.objects.filter(folder=folder.id).first()
                # 判断该用户是否有读的权限
                print(permission)
                if permission is None:
                    folder_data = {
                        'folder': folder.id,
                        'folder_name': folder.name,
                        'permission': -1,
                    }
                    data.append(folder_data)
                elif permission.right is False:
                    folder_data = {
                        'folder': folder.id,
                        'folder_name': folder.name,
                        'permission': 0,
                    }
                    data.append(folder_data)

        return res(action='check permission', data=data)
