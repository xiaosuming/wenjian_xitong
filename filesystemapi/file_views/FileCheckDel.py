from django.views.generic import View
from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator
from filesystemapi.check.file import FileCheck

class FileCheckDel(View):
    """
        @apiName FileCheckDel
        @api {POST} /filesystem/filecheckdel/
        @apiGroup check
        @apiVersion 0.0.1
        @apiDescription 删除文件时，检查文件是否有权限
        @apiParam {list} filesId 文件id列表
        @apiParamExample {form-data} 请求样例：
        {
            "filesId": [1,2],
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Check Rename File",
            "data": {
                "id": [
                    "85",
                    "86"
                ],
                "count": 2
            }
        }
    """
    @method_decorator(httpresult)
    def post(self, request):
        filesId = request.POST.get('filesId')

        # test
        username = 'sgf'
        data = FileCheck.checkdel(self, filesId, username)
        return res(code=1, msg='ERROR', action='Check Delete File', data=data)
