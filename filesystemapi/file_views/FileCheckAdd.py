from django.views.generic import View
from common.res import res
from common.httpresult import httpresult
from django.utils.decorators import method_decorator
from filesystemapi.check.file import FileCheck

class FileCheckAdd(View):
    """
        @apiName fileCheckAdd
        @api {POST} /filesystem/filecheckadd/
        @apiGroup check
        @apiVersion 0.0.1
        @apiDescription 新建文件时，检查文件是否重名
        @apiParam {int} folderId 文件夹id
        @apiParam {string} fileName 文件名
        @apiParamExample {form-data} 请求样例：
        {
            "folderId": 1,
            "fileName": "helloworld",
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Check Add file",
            "data": {
                "access": true,
                "duplicate": false
            }
        }
    """
    @method_decorator(httpresult)
    def post(self, request):
        folderId = request.POST.get('folderId')
        filename = request.POST.get('fileName', '')

        # test
        username = 'sgf'

        data = FileCheck.checkadd(self, folderId, filename, username)

        if data['duplicate'] is False:
            return res(action='Check Add file', data=data)
        else:
            return res(code=1, msg='ERROR', action='Check Add file', data=data)

