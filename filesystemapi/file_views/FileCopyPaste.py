from django.views.generic import View

from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator
from filesystemapi.file_views.function.edit_update import EditUpdate
class FileCopyPaste(View):
    """
        @apiName FileCopyPaste
        @api {POST} /filesystem/filecopypaste/
        @apiGroup file
        @apiVersion 0.0.1
        @apiDescription 文件复制粘贴
        @apiParam {list} srcFilesId 源文件id列表
        @apiParam {int} dstFolderId 目的文件夹id
        @apiParamExample {form-data} 请求样例：
        {
            "srcFilesId": [2,3,4],
            "dstFolderId": 3
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data
        @apiSuccessExample {json} 返回样例:
          {
              "code": 0,
              "msg": "SUCCESS",
              "action": "CopyPaste File",
              "data": [{
                "fileId": 2,
                "fileName": "hello",
                "depth": 3,
              },
              ...
              ]
          }
    """
    @method_decorator(httpresult)
    def post(self, request):
        srcFilesId = request.POST.get('srcFilesId')
        dstFolderId = request.POST.get('dstFolderId')

        data = EditUpdate.filecopypaste(self, srcFilesId, dstFolderId)
        return res(action='CopyPaste File', data=data)
