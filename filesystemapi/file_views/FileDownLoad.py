from django.views.generic import View
from filesystemapi.file_views.function.filedownload import filedownload
from django.http import JsonResponse
class FileDownLoad(View):
    """
        @apiName FileDownload
        @api {GET} /filesystem/filedownload/
        @apiGroup file
        @apiVersion 0.0.1
        @apiDescription 返回url
        @apiParam {list} fileId 文件id列表
        @apiParamExample {params} 请求样例：
        {
            "filesId":[1,2]
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Download File",
            "data": {
                "downloadUrl": xxx/xxx/xxx,
            }
        }
    """

    def get(self, request):
        filesId = request.GET.get('filesId')
        # test
        username = 'sgf'
        response = filedownload(filesId, username)
        if type(response) is dict:
        # if response['code'] == 1:
            return JsonResponse(
                status=500,
                data=response
            )
        return response
