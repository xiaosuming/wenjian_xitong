from django.contrib.auth.models import User
from django.views.generic import View
from django.http import JsonResponse
from common.httpresult import httpresult
from common.getpath import getpath
from common.res import res
from django.utils.decorators import method_decorator
import os


class CheckAcception(View):

    @method_decorator(httpresult)
    def post(self, request):
        print('接受分片开始')
        identifier = request.GET.get('resumableIdentifier')
        chunknumber = request.GET.get('resumableChunkNumber')
        totalchunks = request.GET.get('resumableTotalChunks')
        file = request.FILES.get('file')
        # test
        username = 'sgf'
        data = fileload(identifier, chunknumber, totalchunks, file, username)
        if data['load'] == 0:
            return res(action='UpLoad Chunks', data=data)

        return JsonResponse(
            status=204,
            data=res(data=u'分片已上传')
        )


def fileload(identifier, chunknumber, totalchunks, file, username):

    user = User.objects.filter(username=username).first()

    # 组装uuid
    uuid = identifier
    # 分片名：分片序号
    chunkname = chunknumber
    print('uuid', uuid)
    print('chunkname', chunkname)
    # 生成临时目录
    temp_user_path = getpath('media', 'temp', str(user.username))
    print('temp_user_path:', temp_user_path)

    temp_path = os.path.join(temp_user_path, str(uuid))
    print('temp_path:', temp_path)
    if not os.path.exists(temp_path):
        os.mkdir(temp_path)

    data = {
        'load':1,
        'isFinish': False
    }
    # 新建文件 保存数据
    f_path = os.path.join(temp_path, str(chunkname))
    if os.path.exists(path=f_path):
        data['load'] = 0
        return data
    return data

