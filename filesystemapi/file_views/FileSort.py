from django.views.generic import View

from common.httpresult import httpresult
from common.res import res

from django.utils.decorators import method_decorator

from filesystemapi.file_views.function.edit_search import EditSearch
class FileSort(View):
    """
        @apiName FileSort
        @api {POST} /filesystem/fileSort/
        @apiGroup file
        @apiVersion 0.0.1
        @apiDescription 打开文件夹，同时对文件排序
        @apiParam {int} folderId 父目录id
        @apiParam {string} sortingType 排序类型 {'fileName': 'name', 'fileSize': 'size',
                                    'dateCreated': 'createtime', 'dateModified': 'modifytime'}
        @apiParam {string} sortingOrding 排序方向 descend ascend
        @apiParam {int} page 页码,分页显示排序结果
        @apiParamExample {form-data} 请求样例：
        {
            "folderId": 1,
            "sortingType": "name",
            "sortingOrder": "ascend"，
            "page": 1,
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Sort File",
            "data": {
            "url": "/sgf/其他/其他",
            "depth": 3，
            "isFinished": False,
            'file":
            [
                {
                "fileName": "q1",
                "fileId": 29,
                "dateCreated": "2019-08-02",
                "dateModified": "2019-08-02",
                "fileSize": "0b",
                "filePath": "/sgf/其他/其他",
                "fileType": null，
                },
                ...
            ],
        }
    """
    @method_decorator(httpresult)
    def get(self, request):
        folderId = request.GET.get('folderId')
        sort_type = request.GET.get('sortingType')
        sort_order = request.GET.get('sortingOrder')
        page = request.GET.get('page')
        pagesize = request.GET.get('pageSize')
        data = EditSearch.filesort(self, folderId=folderId, sort_type=sort_type,
                                   sort_order=sort_order, page=page, pagesize=pagesize)
        return res(action='Sort File', data=data)


