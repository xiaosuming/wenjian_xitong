from django.views.generic import View
from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator
from filesystemapi.check.file import FileCheck

class FileCheckRename(View):
    """
        @apiName FileCheckRename
        @api {POST} /filesystem/filecheckrename/
        @apiGroup check
        @apiVersion 0.0.1
        @apiDescription 重命名文件时，检查文件是否重名
        @apiParam {int} fileId 文件id
        @apiParam {string} fileName 文件名
        @apiParamExample {form-data} 请求样例：
        {
            "fileId": 1,
            "fileName": "helloworld",
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Check Rename File",
            "data": {
                "access": true,
                "duplicate": false
            }
        }
    """
    @method_decorator(httpresult)
    def post(self, request):
        fileId = request.POST.get('fileId')
        fileName = request.POST.get('fileName', '')

        # test
        username = 'sgf'

        data = FileCheck.checkrename(self, fileId, fileName, username)

        if data['duplicate'] is False:

            return res(action='Check Rename File', data=data)
        else:
            return res(code=1, msg='ERROR', action='Check Rename File', data=data)
