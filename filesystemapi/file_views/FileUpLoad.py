import hashlib
import os
from functools import partial
from files.models.Folder import Folder
from files.models.File import File
from django.contrib.auth.models import User
from django.views.generic import View
from common.ParamMissingException import ParamMissingException
from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator
from filesystem.settings import CLIENT

class FileUpLoad(View):
    """
        @apiName FileUpLoad
        @api {POST} /filesystem/fileupload/
        @apiGroup file
        @apiVersion 0.0.1
        @apiDescription 组合分片，生成新的文件
        @apiParam {string} folderId 上传所在文件夹
        @apiParam {int} fileId,若有，则覆盖原来的
        @apiParam {String} hashid
        @apiParam {String} name 文件名
        @apiParam {int} totalchunk 总分片数
        @apiParam {String} type 文件类型
        @apiParamExample {form-data} 请求样例：
        {
            。。。
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "UpLoad File",
            "data":
            {
            'fileId': 2,
            'url': "xxx/xxx/xxx",
            'depth': 1,
            }
        }
    """
    # 和片操作
    def packagefile(self, temp_path, files, name):

        # 处理分片
        size = 0
        try:
            for item in files:
                path = temp_path + '/' + item
                with CLIENT.read(path) as reader:
                    features = reader.read()

                    if CLIENT.status(hdfs_path=name, strict=False) is None:
                        print('asdfasdfasdf')
                        CLIENT.write(hdfs_path=name, data=features)
                    CLIENT.write(hdfs_path=name, data=features, append=True)
                    CLIENT.delete(hdfs_path=path, recursive=True)
            CLIENT.delete(hdfs_path=temp_path, recursive=True)
            print('和片成功')
        except Exception:
            CLIENT.delete(hdfs_path=name)
            print('和片失败')
            raise Exception(u'和片失败')

        message = CLIENT.status(hdfs_path=name)
        size = message['length']
        return size

    @method_decorator(httpresult)
    def post(self, request):
        folderId = request.POST.get('folderId')
        identifier = request.POST.get('hashid')
        filename = request.POST.get('name')
        totalchunk = request.POST.get('totalchunk')
        type = request.POST.get('type')
        print(request.POST)
        # test
        username = 'sgf'
        user = User.objects.filter(username=username).first()
        # print('hashid', identifier)
        # print('folderId:', folderId)
        type = str(type).split('/')[1]
        # 不能缺少
        if folderId is None:
            raise ParamMissingException(data=u'缺少父目录id')
        if identifier is None:
            raise ParamMissingException(data=u'缺少identifier')

        # 可选
        if filename is None:
            filename = u'新建文件'

        pfolder = Folder.objects.filter(id=folderId).first()
        if pfolder is None:
            raise Exception(u'父目录不存在')
        fileObjs = File.objects.filter(name=filename, pfolder=pfolder)
        if fileObjs.count() != 0:
            fileObjs.delete()

        temp_path = '/Acceptation/' + identifier

        print('temp_path', temp_path)
        # 检查分片是否完整
        print('检查分片')
        count = int(totalchunk)
        files = CLIENT.list(temp_path)
        print('files', files)
        lens = len(files)
        if count != lens:
            return res(action='Upload File', msg='ERROR', code=1, data=u'分片缺失,传输失败')


        size = self.packagefile(temp_path=temp_path, files=files, name=identifier)

        # 得到depth
        depth = pfolder.depth + 1
        url = pfolder.url
        file_url = os.path.join(url, filename)
        # 数据库中新建file
        fileObj = File.objects.create(name=filename, pfolder=pfolder, url=file_url,
                                      depth=depth, size=size, creater=pfolder.creater,
                                      filetype=type, hdfsName=identifier)

        data = {
            'fileName': fileObj.name,
            'fileId': fileObj.id,
            'dateCreated': fileObj.createtime,
            'dateModified': fileObj.modifytime,
            'fileSize': size,
            'filePath': file_url,
            'fileType': fileObj.filetype

        }
        print('上传完成')
        return res(action='UpLoad File', data=data)