from django.views.generic import View
from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator
import json

from filesystemapi.file_views.function.edit_update import EditUpdate
class FileRename(View):
    """
        @apiName FileRename
        @api {PUT} /filesystem/filerename/
        @apiGroup file
        @apiVersion 0.0.1
        @apiDescription 重命名文件
        @apiParam {String} fileName 文件名
        @apiParam {int} fileId 文件id
        @apiParamExample {json} 请求样例：
        {
            "fileId":2,
            "fileName":"hello"
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data 文件信息
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Rename File",
            "data":{
            "fileId": 2,
            "fileName": "hello",
        }
    """
    @method_decorator(httpresult)
    def put(self, request):
        data = json.loads(request.body)
        fileId = data.get('fileId')
        # 文件名需后缀
        filename = data.get('fileName')
        # test
        username = 'sgf'
        data = EditUpdate.filerename(self, fileId, filename, username)
        return res(action='Rename File', data=data)





