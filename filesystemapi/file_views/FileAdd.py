from django.views.generic import View
from common.httpresult import httpresult
from common.res import res
from django.utils.decorators import method_decorator
from filesystemapi.file_views.function.edit_create import EditCreate


class FileAdd(View):
    """
        @apiName FileAdd
        @api {POST} /filesystem/fileadd/
        @apiGroup file
        @apiVersion 0.0.1
        @apiDescription 新建文件
        @apiParam {String} fileName 文件名
        @apiParam {int} pfolder 父目录id
        @apiParamExample {form-data} 请求样例：
        {
            "pfolder":2,
            "fileName":"hello"
        }
        @apiSuccess (200) {String} msg 信息
        @apiSuccess (200) {String} code 0代表无错误 1代表有错误
        @apiSuccess (200) {json} data 文件信息
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "action": "Add File",
            "data":{
            "fileId": 2,
            "fileName": "hello",
            "depth": 1,
            }
        }
    """
    @method_decorator(httpresult)
    def post(self, request):
        pfolderId = request.POST.get('pfolderId')
        # 文件名需后缀
        filename = request.POST.get('fileName')
        # test
        username = 'sgf'
        data = EditCreate.fileadd(self, pfolderId, filename, username)
        return res(action='Add File', data=data)





