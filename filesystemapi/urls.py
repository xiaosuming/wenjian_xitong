from django.urls import path

from filesystemapi.file_views.FileCopyPaste import FileCopyPaste
from filesystemapi.file_views.FileDel import FileDel
from filesystemapi.file_views.FileRename import FileRename
from filesystemapi.file_views.FileAcception import FileAcception
from filesystemapi.file_views.FileUpLoad import FileUpLoad
from filesystemapi.file_views.FileDownLoad import FileDownLoad
from filesystemapi.file_views.FileCutPaste import FileCutPaste
from filesystemapi.file_views.FileSort import FileSort
from filesystemapi.file_views.FileAdd import FileAdd
from filesystemapi.file_views.FileSelectUrl import FileSelectUrl
from filesystemapi.file_views.CheckAcception import CheckAcception

from filesystemapi.folder_views.FolderAdd import FolderAdd
from filesystemapi.folder_views.FolderView import FolderView
from filesystemapi.folder_views.FolderDelete import FolderDelete
from filesystemapi.folder_views.FolderCopyPaste import FolderCopyPaste
from filesystemapi.folder_views.FolderRename import FolderRename
from filesystemapi.folder_views.FolderCutPaste import FolderCutPaste
from filesystemapi.folder_views.GetFolderInfo import GetFolderInfo
from filesystemapi.folder_views.FolderUrl import FoldersUrl
from filesystemapi.views.UserView import UserView
from filesystemapi.folder_views.FolderDownLoad import FolderDownLoad
from filesystemapi.views.ShareAdd import ShareAdd
from filesystemapi.views.ShareManage import ShareManage
from filesystemapi.views.ShareDel import ShareDel
from filesystemapi.views.ShareModify import ShareModify
from filesystemapi.views.SearchCommon import SearchCommon
from filesystemapi.views.SearchManage import SearchManage
from filesystemapi.views.SearchSameFolderFile import SearchSameFolderFile
from filesystemapi.views.test import Test
from filesystemapi.views.test1 import Test1

from filesystemapi.folder_views.FolderCheckRename import FolderCheckRename
from filesystemapi.folder_views.FolderCheckAdd import FolderCheckAdd
from filesystemapi.folder_views.FolderCheckPaste import FolderCheckPaste
from filesystemapi.folder_views.FolderCheckDel import FolderCheckDel
from filesystemapi.file_views.FileCheckRename import FileCheckRename
from filesystemapi.file_views.FileCheckAdd import FileCheckAdd
from filesystemapi.file_views.FileCheckPaste import FileCheckPaste
from filesystemapi.file_views.FileCheckDel import FileCheckDel

from filesystemapi.views.SignView import SignView
from filesystemapi.views.PhotoModify import PhotoModify
from filesystemapi.views.PhotoModify2 import PhotoModify2
from filesystemapi.views.PhotoModifyAll import PhotoModifyAll

from filesystemapi.views.LabelFile import LabelFile
from filesystemapi.views.GetPath import GetPath
from filesystemapi.views.GetFile import GetFile
from filesystemapi.views.SearchFile import SearchFile
from filesystemapi.views.SearchAllFile import SearchAllFile

urlpatterns = [
    path('checkacception/', CheckAcception.as_view()),
    path('fileload/', FileAcception.as_view()),
    path('fileupload/', FileUpLoad.as_view()),
    path('filedownload/', FileDownLoad.as_view()),
    path('filecutpaste/', FileCutPaste.as_view()),
    path('fileadd/', FileAdd.as_view()),
    path('filecopypaste/', FileCopyPaste.as_view()),
    path('filedel/', FileDel.as_view()),
    path('filerename/', FileRename.as_view()),
    path('filesort/', FileSort.as_view()),
    path('filesurl/', FileSelectUrl.as_view()),
    path('folderadd/', FolderAdd.as_view()),
    path('folderview/', FolderView.as_view()),
    path('folderrename/', FolderRename.as_view()),
    path('folderdel/', FolderDelete.as_view()),
    path('foldercopypaste/', FolderCopyPaste.as_view()),
    path('foldercutpaste/', FolderCutPaste.as_view()),
    path('folderdownload/', FolderDownLoad.as_view()),
    path('foldersurl/', FoldersUrl.as_view()),
    path('viewallfolders/', GetFolderInfo.as_view()),
    path('userview/', UserView.as_view()),
    path('shareadd/', ShareAdd.as_view()),
    path('sharemanage/', ShareManage.as_view()),
    path('sharemodify/', ShareModify.as_view()),
    path('sharedel/', ShareDel.as_view()),
    path('searchcommon/', SearchCommon.as_view()),
    path('searchmanage/', SearchManage.as_view()),
    path('searchsff/', SearchSameFolderFile.as_view()),
    path('foldercheckrename/', FolderCheckRename.as_view()),
    path('foldercheckadd/', FolderCheckAdd.as_view()),
    path('foldercheckpaste/', FolderCheckPaste.as_view()),
    path('foldercheckdel/', FolderCheckDel.as_view()),
    path('filecheckrename/', FileCheckRename.as_view()),
    path('filecheckadd/', FileCheckAdd.as_view()),
    path('filecheckpaste/', FileCheckPaste.as_view()),
    path('filecheckdel/', FileCheckDel.as_view()),
    path('signview/', SignView.as_view()),
    path('photomodify/', PhotoModify.as_view()),
    path('photomodify2/', PhotoModify2.as_view()),
    path('photomodifyall/', PhotoModifyAll.as_view()),
    path('labelfile/', LabelFile.as_view()),
    path('getpath/', GetPath.as_view()),
    path('getfile/', GetFile.as_view()),
    path('searchfile/', SearchFile.as_view()),
    path('searchallfile/', SearchAllFile.as_view()),
    path('test/', Test.as_view()),
    path('test1/', Test1.as_view()),
]

