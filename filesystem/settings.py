import os
import logging
from sendfile import sendfile
from hdfs import *
from .logconfig import LOGGINGS
from .config import *

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# print(BASE_DIR)


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'm0&n*6+h^eq#$kl9duldxx-90mio$7rngzobtpdd(^&niz1klm'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'files',
]

CORS_ALLOW_CREDENTIALS = True
CORS_ORIGIN_ALLOW_ALL = True
CORS_ORIGIN_WHITELIST = (
    '*',
)

CORS_ALLOW_METHODS = (
    'DELETE',
    'GET',
    'OPTIONS',
    'PATCH',
    'POST',
    'PUT',
    'VIEW',
)

CORS_ALLOW_HEADERS = (
    'XMLHttpRequest',
    'X_FILENAME',
    'accept-encoding',
    'authorization',
    'content-type',
    'dnt',
    'origin',
    'user-agent',
    'x-csrftoken',
    'x-requested-with',
    'Pragma',
    'UAT',
    'uat',
    'Uat',
    'jwt',
    'JWT',
)


ROOT_URLCONF = 'filesystem.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'files/../templates')]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'filesystem.wsgi.application'

# 环境判断
if os.environ.get('CONFIGURATION') == 'test':
    # hdfs配置
    CLIENT = dev_hdfs()
    # 中间件配置
    MIDDLEWARE = middleware
    # 数据库配置
    DATABASES = test
    DEBUG = False
    # 日志配置
    LOGGING = LOGGINGS
    # redis配置
    REDIS = redis
    # 认证服务器地址
    verifyUATURI = "http://192.168.50.156:30002/oauth/uatverifior/"
    IP = 'http://192.168.50.128:30003/filesystem/getfile/?path='
elif os.environ.get('CONFIGURATION') == 'staging':
    CLIENT = staging_hdfs()
    MIDDLEWARE = middleware
    DATABASES = staging
    DEBUG = False
    LOGGING = LOGGINGS
    # redis配置
    REDIS = redis
    verifyUATURI = "http://192.168.50.156:30002/oauth/uatverifior/"
    IP = 'http://192.168.50.128:30003/filesystem/getfile/?path='
else:
    CLIENT = local_hdfs()
    MIDDLEWARE = local_middleware
    DATABASES = local
    DEBUG = True
    REDIS = redis
    verifyUATURI = "http://192.168.50.104:8000/oauth/uatverifior/"
    IP = 'http://192.168.50.48:8082/filesystem/getfile/?path='

# 验证失败重定向问题
OAUTH_AUTHORIZE_URI = '192.168.50.156:30002/oauth/authorize/?response_type=code&client_id=123456'

#
# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'zh-Hans'

TIME_ZONE = 'Asia/Shanghai'

USE_I18N = True

USE_L10N = True

USE_TZ = False


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/static/'
MEDIA_URL = '/media/'
# STATIC_ROOT = 'static'

# 路径分割符
URL_SEPARTOR = os.sep

# 文件基础路径
PROJECT_BASE_PATH = os.getcwd()
# FILE_TEMP_BASE_PATH = os.path.join(PROJECT_BASE_PATH, 'temp')

# 基础路径
URL_BASE_PATH = os.path.join(PROJECT_BASE_PATH, 'media')

APPEND_SLASH=False

SENDFILE_BACKEND = "sendfile.backends.development"

