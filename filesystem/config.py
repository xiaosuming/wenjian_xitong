# redis配置
redis = {
    'HOST':'192.168.50.156',
    'PASSWORD': '123456',
    'PORT': '6379',
}
# 数据库配置
local = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'filesystem',
        'USER': 'root',
        'PASSWORD': '12345678',
        'HOST': '172.17.0.1',
        'PORT': '3306',
    },

}

test = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'filesystem',
        'USER': 'wangchen',
        'PASSWORD': '123456',
        'HOST': '192.168.50.37',
        'PORT': '3306',
    },
}
staging = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'filesystem',
        'USER': 'wangchen',
        'PASSWORD': '123456',
        'HOST': '192.168.50.156',
        'PORT': '3306',
    },
}

# 集群中间件配置
middleware = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'filesystemapi.middlewares.CheckJWTMiddleware.CheckJWTMiddleware',
    'filesystemapi.middlewares.AuthorizeRequiredMiddleware.AuthorizeRequiredMiddleware',
    'filesystemapi.middlewares.CheckResponseMiddleware.CheckResponseMiddleware'

]
# 本地中间件
local_middleware = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
]
from hdfs import *
# hdfs配置
def local_hdfs():
    HDFSCLI_TEST_URL = 'http://172.17.0.1:50070'
    CLIENT = InsecureClient(url=HDFSCLI_TEST_URL, user='filesystem')
    if CLIENT.status(hdfs_path='/filesystem/Acceptation', strict=False) is None:
        CLIENT.makedirs(hdfs_path='/filesystem/Acceptation')
        print('Acceptation文件夹已创建')
    print(u"hdfs连接成功")
    return CLIENT

def dev_hdfs():
    HDFSCLI_TEST_URL = 'http://192.168.50.156:50070'
    CLIENT = InsecureClient(HDFSCLI_TEST_URL, user='filesystem')
    if CLIENT.status(hdfs_path='/filesystem/Acceptation', strict=False) is None:
        CLIENT.makedirs(hdfs_path='/filesystem/Acceptation')
        print('Acceptation文件夹已创建')
    print(u"hdfs连接成功")
    return CLIENT

def staging_hdfs():
    HDFSCLI_TEST_URL = 'http://192.168.50.128:50070'
    CLIENT = InsecureClient(HDFSCLI_TEST_URL, user='filesystem')
    if CLIENT.status(hdfs_path='/filesystem/Acceptation', strict=False) is None:
        CLIENT.makedirs(hdfs_path='/filesystem/Acceptation')
        print('Acceptation文件夹已创建')
    print(u"hdfs连接成功")
    return CLIENT