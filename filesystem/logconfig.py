import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# 创建日志文件夹路径
LOG_PATH = os.path.join(BASE_DIR, 'media/log')
# 如过地址不存在，则自动创建log文件夹
if not os.path.isdir(LOG_PATH):
    os.mkdir(LOG_PATH)

LOGGINGS = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '{levelname} {asctime} {module} {process:d} {thread:d} {message}',
            'style': '{',
        },
        'simple': {
            'format': '{levelname} {asctime} {module} {message}',
            'style': '{',
        },
    },
    # 'filters': {
    #     # 'special': {
    #     #     '()': 'filesystem.logging.SpecialFilter',
    #     #     'foo': 'bar',
    #     # },
    #     'require_debug_true': {
    #         '()': 'django.utils.log.RequireDebugTrue',
    #     },
    # },
    'handlers': {
        'console': {
            'level': 'INFO',
            # 'filters': ['require_debug_true'],
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'simple',
            'filename': '%s/log.txt' % LOG_PATH,
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': True,
        },
    }
}